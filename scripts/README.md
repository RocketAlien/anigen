This folder contains various scripts for supporting the anitar sprite generation service.

One of the scripts will pull 3D model data from S3 and copy data into a local directory,
creating a separate folder for each media pack.

Another script will create an Ogre resources.cfg file for consuption by Ogre resource manager
on server startup.

Another script will launch the anitar sprite generation service and restart it in case of a
caught/uncaught signal, access violation or some other hard failure.
