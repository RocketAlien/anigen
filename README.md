Overview
========
This directory contains source code for the avatar sprite generation service called anigen.
The code is broken down into modules, each implementing a separate area of functionality.

- Server module handles requests and calls other modules to render avatar sprites.
- Renderer module is responsible for rendering avatar sprites and avatar animation sprite sheets,
  as well as encoding them into a variety of image formats, including JPEG, PNG, BMP and TIFF.
- Client module is actually a collection of anigen clients implemented in different lanuages.

Dependencies
============
Anigen has many dependencies, information on some of them is provided below.

- The service was built on a 32-bit machine running Ubuntu Linux 10.10
- Latest NVIDIA graphics drivers for Linux, 32-bit, version 270.41.19, downloaded from nvidia.com
- cmake and cmake-qt-gui Ubuntu packages, MacPorts might have different package names
- Boost C++ libraries downloaded from boost.org, built/installed from source
- Extension to the boost GIL library currently under review by the boost committee,
  header-only library is checked into anigen source tree under src/include
- Thrift RPC compiler and libraries: libthrift, libthriftnb, downloaded from thrift.apache.org,
  built/installed from source
- Ogre 3D rendering engine libraries and modules: libOgreMain, downloaded from ogre3d.org,
  built/installed from source
- The following page describes all the dependencies you need to build Ogre:
  http://www.ogre3d.org/tikiwiki/Prerequisites?tikiversion=Linux
- Jansson JSON parser library: libjansson, downloaded from digip.org/jansson,
  built/installed from source
- Other libraries: libevent, libpthread, libpng12, libjpeg, libtiffxx

Building Anigen
===============

In Ubuntu Linux do the following when setting up a new machine for the first time:

    $ cd build
    $ make prereq
    $ sudo make prereq_install 

To compile changes to Anigen:

    $ cd build
    $ make
    $ make install
    $ make assets

Compiling Options
=================
Compile the service by starting a command prompt and running make from the build directory.
The build process should produce an executable called anigen located in the bin directory.
By default, the build process creates a release version of the servic ebinary. The following is a full list of build targets and corresponding descriptions.

    make rel | make     - build a release version of the executable
    make dbg            - build a debug version of the executable
    make install        - build the executable and put all script files into the bin directory
    make assets         - download all assets by syncing against git repo and
                          create assets package and ogre resources file
    make assets_clean   - delete assets package and ogre resources file
    make clean          - delete binary object files and the executable
    make gen_clean      - delete generated C++ thrift files
    make bin_clean      - delete the bin directory
    make pristine       - run all clean targets

Configuring the Service
=======================
The service can be configured by providing parameters on the command line or by specifying them in a configuration file.

Configuration parameters are type/name/value triples of the following form.

    {s|i|f}:{$name}={$value}
    where s=string, i=integer, f=float

Separate configuration parameters with spaces when specifying them on the command line, or with newlines when specifying them in a configuration file.

Configuration parameters include the following:

    i:port=9090                     - server port, default=9090
    i:width=400                     - rendering surface width (single frame),
                                      min=80, max=800, default=640
    i:height=300                    - rendering surface height (single frame),
                                      min=60, max=600, default=480
    i:test=1                        - start server in test mode, default=0
    s:config=anigen.cfg             - server configuration file name, default=anigen.ini
    s:ogre.config=config.cfg        - Ogre configuration file name, default=ogre.config
    s:ogre.plugins=plugins.cfg      - Ogre plugins file name, default=ogre.plugins
    s:ogre.resources=resources.cfg  - Ogre resource file name, default=ogre.resources

Running the Service
===================
Start anigen by launching the compiled executable from the command line.

The service has been implemented to run as a daemon so that a user logging off the machine will not cause the service to terminate.

Upon startup, the service will log its configuration as well as any diagnostic messages to standard output, and a wrapper script will be implemented later to redirect log messages to an appropriate place or external service.

The service can be stopped by pressing `Ctrl+C` at the command prompt, sending it a `SIGTERM(15)` signal for graceful termination or sending it a `SIGKILL(9)` signal for immediate termination.

Upon graceful shutdown (`Ctrl+C` or `SIGTERM`), the service will free any acquired resources, output its accumulated statistics to standard output and terminate.

Anigen Command Line Interface
=============================
When you build anigen server, a node.js command line client called anigen_cli.js is built as well.

Here's a typical command line invocaton of the anigen CLI.

    $ anigen_cli.js --host=localhost --port=9090 --api=version

Here's a complete list of APIs, see Thrift schema under src/thrift for API parameters.

    --api=ping      - ping anigen server, OK on success, or error message
    --api=kill      - kill anigen server, OK on success, or error message
    --api=reload    - reload anigen assets, OK on success, or error message
    --api=version   - get server version, version on success, or error message
    --api=config    - get server configuration, configuration on success, or error message
    --api=stats     - get server statistics, statistics on success, or error message
    --api=avatar    - render avatars in default poses, image binary is output to stdout, or error message
    --api=pose      - render avatars in specific poses, image binary is output to stdout, or error message
    --api=animation - render avatar animations, image binary is output to stdout, or error message
    
Boost `GIL io_new` Extension
==========================
The include directory contains a header-only extension to the boost GIL library which provides
a stream-oriented interface as opposed to the old file-based interface for reading/writing
various image formats.

The library has test code and test images stripped out to reduce the size.

Here's the direct link for downloading the extension:

[http://gil-contributions.googlecode.com/files/boost_review.zip](http://gil-contributions.googlecode.com/files/boost_review.zip)

Here's the main page for the Google project GIL contributions page:

[http://code.google.com/p/gil-contributions/](http://code.google.com/p/gil-contributions/)

Here are some links describing the extension in more detail:

[http://binglongx.wordpress.com/2010/07/09/boost-gil-contributions-extensions-and-io/](http://binglongx.wordpress.com/2010/07/09/boost-gil-contributions-extensions-and-io/)

[http://gil-contributions.googlecode.com/svn/trunk/gil_2/libs/gil/io_new/doc/io.qbk](http://gil-contributions.googlecode.com/svn/trunk/gil_2/libs/gil/io_new/doc/io.qbk)

Here are some links describing the boost GIL library in more detail:

[http://www.boost.org/doc/libs/1_49_0/libs/gil/doc/html/gildesignguide.html](http://www.boost.org/doc/libs/1_49_0/libs/gil/doc/html/gildesignguide.html)

[http://stlab.adobe.com/gil/html/giltutorial.html](http://stlab.adobe.com/gil/html/giltutorial.html)

TODO List
=========
- Find out what compiler optimization level is appropriate as we want to retain the ability to
  debug a live production system while enabling some level of optimization.
- Create a script to start anigen service and to restart it whenever there is a hard error.
  The service should gracefully terminate when encountering several hard errors, including
  access violations, floating-point exception, Ctrl+C or SIGTERM signals.
- When initializing the Ogre RenderSystem, simply create an instance of the OpenGL RenderSystem
  instead of loading renderers through the plugin mechanism and going through the list of
  available renderers.
- Run the following command from the anigen source directory to get the list of all TODO items.

    grep -R "todo:" *
