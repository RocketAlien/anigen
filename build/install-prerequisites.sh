#!/bin/sh
#-*- shell -*- -p
######################################################################
# automated build script for building anigen on ubuntu
######################################################################

# include utility functions
. 'utility.sh'

######################################################################
# the "jobs" part of the script
######################################################################

# base linux build
runCMD "mkdir -p $HOME/temp"

cd $HOME/temp

labelEcho 'Install Build tools'
aptgetInstall 'build-essential automake libtool cmake cmake-qt-gui'
aptgetInstall 'curl libcurl3 libcurl3-dev' # make sure we have curl as well since we use it to download

labelEcho 'Install Boost'
aptgetInstall 'libbz2-dev' # for boost 
#aptgetInstall 'libboost-all-dev'
download2Bootstrap 'http://sourceforge.net/projects/boost/files/boost/1.49.0/boost_1_49_0.tar.bz2'

labelEcho 'Build JANSSON'
download2Make 'http://www.digip.org/jansson/releases/jansson-2.3.tar.gz'

labelEcho 'Build Thrift'
download2Make 'http://apache.mirrors.redwire.net//thrift/0.8.0/thrift-0.8.0.tar.gz'

labelEcho 'Ogre Dependencies'
aptgetInstall 'libfreetype6-dev libfreeimage-dev libzzip-dev libxrandr-dev libxaw7-dev freeglut3-dev libgl1-mesa-dev libglu1-mesa-dev'
aptgetInstall 'libxt-dev'
aptgetInstall 'libpng3-dev'
aptgetInstall 'nvidia-cg-toolkit libois-dev libboost-thread-dev'

labelEcho 'install libtiffxx & libjpeg'
aptgetInstall 'libjpeg-dev'

labelEcho 'install libevent-dev'
aptgetInstall 'libevent-dev'

labelEcho 'install libglew-dev'
aptgetInstall 'libglew-dev'

# optional ogre dependencies
# aptgetInstall 'doxygen graphviz libcppunit-dev'

labelEcho 'Build OGRE'
download2CMake 'http://sourceforge.net/projects/ogre/files/ogre/1.8/1.8.0-RC1/ogre_src_v1-8-0RC1.tar.bz2'

# this is not ogre dependency
labelEcho 'install libtiff4-dev'
aptgetInstall 'libtiff4-dev'

echo 'Done'
