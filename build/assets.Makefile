_repo_root = ..
_bin_root = $(_repo_root)/bin

ASSET_REPO = ../assets

_zip_file = assets.zip

all: zip resources

update:
	cd $(_repo_root) && git submodule update --init --recursive

zip: update
	zip -r $(_bin_root)/$(_zip_file) $(ASSET_REPO) -x $(ASSET_REPO)/.git\*

resources: 
	cd $(_bin_root) && echo [General] > ogre.resources
	cd $(_bin_root) && echo Zip=$(_zip_file) >> ogre.resources

clean:
	rm -f $(_bin_root)/$(_zip_file)
	rm -f $(_bin_root)/ogre.resources

.PHONY: all update zip resources clean
