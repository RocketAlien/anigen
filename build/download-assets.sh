#!/bin/sh
ASSETS_FILENAME=${1:-assets.zip}
GITHUB_USERNAME=${2:-$GITHUB_USERNAME}
GITHUB_PASSWORD=${3:-$GITHUB_PASSWORD}

[ -z $ASSETS_FILENAME ] && { echo ASSETS_FILENAME is not set; exit 1; }
[ -z $GITHUB_USERNAME ] && { echo GITHUB_USERNAME is not set; exit 1; }
[ -z $GITHUB_PASSWORD ] && { echo GITHUB_PASSWORD is not set; exit 1; }

curl -L https://$GITHUB_USERNAME:$GITHUB_PASSWORD@github.com/appuri/anigen.assets/zipball/master -o $ASSETS_FILENAME

exit 0
