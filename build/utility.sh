#!/bin/sh
#-*- shell -*- -p
######################################################################
# generic functions for inclusion in other build scripts
######################################################################

# print a visible message
labelEcho() {
    echo '#########################################'
    echo "$@"
    echo '#########################################'
}

# print a message & exit.
fail() {
    labelEcho "ERROR [$2]: $1"
    exit $2
}

# test to see if the status is OK, if not - exit.
ensureOK() {
    local OK=0
    if [ $? -ne $OK ]
    then 
        fail "$1" $? # need quotes on MSG to pass a whole string as 1 item
    fi
}

# run a shell command (and print a label for it)
Exec() {
    local CMD="$@" # needed to pull in a whole string.
    labelEcho "exec: $CMD"
    $CMD
}

# run a shell command and test the status code
runCMD() {
    Exec "$@"
    ensureOK "$CMD" # quote is needed to passed it forward as one item
}

# takes a directory and ensure it's clean (by dropping and recreating)
refreshPath() {
    local DIR="$1"
    if [ -d $DIR ]; then
        if [ -L $DIR ]; then
            runCMD "rm $DIR"
        else
            runCMD "rm -rf $DIR"
        fi
    fi
    runCMD "mkdir -p $DIR"
}

# install debian packages silently (except the first sudo)
aptgetInstall() {
    runCMD "sudo apt-get install -q -y $1"
}

# return file extension
fileExt() {
    echo "${1##*.}"
}

# return the file name part - works for regular files & urls
filename() {
    local AFTER_SLASH=${1##*/}
    echo "${AFTER_SLASH%%\?*}"
}

# return a random number
random() {
    hexdump -n 2 -e '/2 "%u"' /dev/urandom
}

# generate a random filename
randomFilename() {
    echo "${1}$(random)${2}"
}

# based on the extension determine the appropriate flag for tar
tarExtOpt() {
    local EXT=$(fileExt "$1")
    if [ "$EXT" = 'gz' ]; then
        echo 'zxvf'
        return
    elif [ "$EXT" = 'bz2' ]; then
        echo 'jxvf'
        return
    else
        # error condition.
        fail "Unknown tar extension type: $EXT", 1
    fi
}

# download a file
# this is designed to support "skipping" the download if
# the download has already occured.
download() {
    local CURL_HTTP_RANGE_ERROR=33
    local TEMP_FILE="._temp_${2}"
    if [ -f $2 ]
    then
        labelEcho "download $2: exists"
    else
        Exec "curl -L -C - -o $TEMP_FILE $1"
        if [ $? -eq $CURL_HTTP_RANGE_ERROR ]
        then
        # we will fully overwrite the initial download
            runCMD "curl -L -o $TEMP_FILE $1"
        fi
        ensureOK "download"
        runCMD "mv $TEMP_FILE $2"
    fi
}

# untar a file based on the extension - NOTE the FILE MUST have an extension
extract() {
    local ARCHIVE="$1"
    local TARGET="$2"
    local OPT=$(tarExtOpt $ARCHIVE)
    labelEcho "extract $1 $2"
    refreshPath $TARGET
    runCMD "tar $OPT $ARCHIVE --strip-components=1 -C $TARGET"
}

# the process of make
Make() {
    cd $1
    if [ -f "./configure" ]
    then
        runCMD "./configure"
    fi
    runCMD "make"
    runCMD "sudo make install"
    cd ..
    runCMD "rm -rf $1"
}

# the process of cmake - note the separate build directory
CMake() {
    local DIR=$1
    local BUILD_DIR=$(randomFilename ".cmake_")
    refreshPath $BUILD_DIR
    cd $BUILD_DIR
    runCMD "cmake ../$DIR"
    runCMD "make"
    runCMD "sudo make install"
    cd ..
    runCMD "rm -rf $DIR"
    runCMD "rm -rf $BUILD_DIR"
}

Bootstrap() {
    local DIR=$1
    cd $DIR
    runCMD "./bootstrap"
    runCMD "./b2"
    runCMD "sudo ./b2 install"
    cd ..
    runCMD "rm -rf $DIR"
}

download2Build() {
    local TARGET=$(filename $1)
    local BUILD=$2
    download $1 $TARGET
    local MAKE_DIR=$(randomFilename ".make_")
    extract $TARGET $MAKE_DIR
    $BUILD $MAKE_DIR
}

# wrapping the download + make
download2Make() {
    download2Build "$1" Make
}

# wrapping the download + cmake
download2CMake() {
    download2Build "$1" CMake
}

download2Bootstrap() {
    download2Build "$1" Bootstrap
}
