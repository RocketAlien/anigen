# Required to be passed in
# URL
# CONFIGURE
# MAKE

_build := $(MAKE) 
_file := $(notdir $(URL))
_temp_file := .$(_file)
_dir := $(notdir $(basename $(basename $(_file))))
_temp_dir := .$(_dir)
_done_config := .$(_dir).done_config
_done_make := .$(_dir).done_make
_ext := $(suffix $(_file))
ifeq ($(_ext), .bz2)
	_tar_flag := -jxvf
else
	_tar_flag := -zxvf
endif
ifdef BUILDDIR
	_build_dir := $(BUILDDIR)
else
	_build_dir := $(_dir)
endif

_temp_build_dir := .$(_build_dir)

ifeq ($(CONFIGURE), cmake)
	_configure := $(CONFIGURE) ../$(_dir)
else
	_configure := $(CONFIGURE)
endif 

all: build

build: $(_done_make)

$(_file): 
	curl -L -o $(_temp_file) $(URL)
	mv $(_temp_file) $(_file)

$(_dir): $(_file)
	mkdir -p $(_temp_dir)
	tar $(_tar_flag) $(_file) --strip-components=1 -C $(_temp_dir)
	mv $(_temp_dir) $(_dir)

# _build_dir might be the same as _dir
$(_build_dir): $(_dir)
ifneq ($(_build_dir), $(_dir))
	mkdir -p $(_temp_build_dir)
	mv $(_temp_build_dir) $(_build_dir)
endif

$(_done_config): $(_build_dir) 
	cd $(_build_dir) && $(_configure)
	touch $(_done_config)

$(_done_make): $(_done_config) 
	cd $(_build_dir) && $(_build)
	touch $(_done_make)

download: $(_file)

extract: $(_dir)

config: $(_done_config)

install: build
	cd $(_build_dir) && $(_build) install

clean:
	cd $(_build_dir) && $(_build) clean
	rm -f $(_done_config)
	rm -f $(_done_make)

pristine: clean
	rm -f $(_file)
	rm -rf $(_dir)

var:
	echo _temp_file = $(_temp_file)
	echo _file = $(_file)
	echo _temp_dir = $(_temp_dir)
	echo _dir = $(_dir)
	echo _done_config = $(_done_config)
	echo _done_make = $(_done_make)

.PHONY: all clean pristine install download extract config var
