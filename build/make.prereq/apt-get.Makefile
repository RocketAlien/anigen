build := build-essential automake libtool cmake cmake-qt-gui bison flex

curl := curl libcurl3 libcurl3-dev

boost := libbz2-dev

ogre := libfreetype6-dev libfreeimage-dev libzzip-dev libxrandr-dev libxaw7-dev 
ogre += freeglut3-dev libgl1-mesa-dev libglu1-mesa-dev libxt-dev
ogre += libpng3-dev nvidia-cg-toolkit libois-dev libboost-thread-dev

anigen := libjpeg-dev libevent-dev libglew-dev libtiff4-dev

ALL := $(build) $(curl) $(boost) $(ogre) $(anigen)

all: $(ALL)

$(ALL):
	sudo apt-get install -q -y $@
