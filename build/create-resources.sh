#!/bin/sh
ASSETS_FILENAME=${1:-assets.zip}
RESOURCES_FILENAME=${2:-ogre.resources}

[ -z $ASSETS_FILENAME    ] && { echo ASSETS_FILENAME is not set;    exit 1; }
[ -z $RESOURCES_FILENAME ] && { echo RESOURCES_FILENAME is not set; exit 1; }

echo [General] > $RESOURCES_FILENAME
echo Zip=$ASSETS_FILENAME >> $RESOURCES_FILENAME

exit 0
