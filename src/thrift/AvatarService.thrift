namespace cpp appuri.avatar

struct ServerVersion {
    1:required i32 versionMajor;
    2:required i32 versionMinor;
}

struct ServerConfig {
    1:required i32 frameWidth;
    2:required i32 frameHeight;
    3:required i32 testMode;
    4:required string configFile;
    5:required string ogreConfigFile;
    6:required string ogrePluginsFile;
    7:required string ogreResourcesFile;
}

struct ServerStats {
    1:required double upTimeSeconds;
    2:required double renderTimeSeconds;
    3:required i64 numFrames;
}

service AvatarService {
    void ping();
    void kill();
    void reloadAssets();

    ServerVersion getServerVersion();
    ServerConfig  getServerConfig ();
    ServerStats   getServerStats  ();

    binary renderAvatars(1:list<string> avatarDescriptions,
                         2:string       imageDescription);

    binary renderAvatarPoses(1:list<string> avatarDescriptions,
                             2:list<string> avatarPoses,
                             3:string       imageDescription);

    list<binary> renderAvatarAnimations(1:list<string> avatarDescriptions,
                                        2:list<string> avatarAnimations,
                                        3:string       imageDescription);
}

//server constants
const i32       DEFAULT_SERVER_PORT                 = 9090;
const i32       DEFAULT_SERVER_TEST_MODE            = 0;
const string    DEFAULT_SERVER_CONFIG               = "anigen.ini";

//renderer constants
const string    DEFAULT_OGRE_CONFIG                 = "ogre.config";
const string    DEFAULT_OGRE_PLUGINS                = "ogre.plugins";
const string    DEFAULT_OGRE_RESOURCES              = "ogre.resources";
const i32       MAX_NUM_AVATARS                     = 2;
const i32       DEFAULT_NUM_AVATARS                 = 2;

//avatar description constants
//physical features
const double    DEFAULT_AVATAR_HEIGHT               = 1.0;
const double    DEFAULT_AVATAR_GIRTH                = 1.0;
const string    DEFAULT_AVATAR_GENDER               = "male";
const i32       DEFAULT_AVATAR_BODYSHAPE            = 1;
const i32       DEFAULT_AVATAR_HEAD                 = 1;
const i32       DEFAULT_AVATAR_SKINCOLOR            = 1;
const i32       DEFAULT_AVATAR_HAIRSTYLE            = 1;
const i32       DEFAULT_AVATAR_HAIRCOLOR            = 1;
const i32       DEFAULT_AVATAR_EYEBROWS             = 1;
const i32       DEFAULT_AVATAR_EYEBROWCOLOR         = 1;
const i32       DEFAULT_AVATAR_EYES                 = 1;
const i32       DEFAULT_AVATAR_EYECOLOR             = 1;
const i32       DEFAULT_AVATAR_EYESHADOW            = 1;
const i32       DEFAULT_AVATAR_EARS                 = 1;
const i32       DEFAULT_AVATAR_NOSE                 = 1;
const i32       DEFAULT_AVATAR_MOUTH                = 1;
const i32       DEFAULT_AVATAR_MOUTHCOLOR           = 1;
const i32       DEFAULT_AVATAR_CHIN                 = 1;
const i32       DEFAULT_AVATAR_FACIALHAIR           = 0;
const i32       DEFAULT_AVATAR_FACIALMARKS          = 0;
//accessories
const i32       DEFAULT_AVATAR_DRESS                = 0;
const i32       DEFAULT_AVATAR_TOP                  = 1;
const i32       DEFAULT_AVATAR_BOTTOM               = 1;
const i32       DEFAULT_AVATAR_SHOES                = 1;
const i32       DEFAULT_AVATAR_HEADWEAR             = 0;
const i32       DEFAULT_AVATAR_GLASSES              = 0;
const i32       DEFAULT_AVATAR_EARRINGS             = 0;
const i32       DEFAULT_AVATAR_NECKLACE             = 0;
const i32       DEFAULT_AVATAR_WRISTWEAR            = 0;
const i32       DEFAULT_AVATAR_RINGS                = 0;
const i32       DEFAULT_AVATAR_GLOVES               = 0;
//position/orientation
const double    DEFAULT_AVATAR_POS_X                = 0.0;
const double    DEFAULT_AVATAR_POS_Y                = 0.0;
const double    DEFAULT_AVATAR_POS_Z                = 0.0;
const double    DEFAULT_AVATAR_ROT_AXIS_X           = 0.0;
const double    DEFAULT_AVATAR_ROT_AXIS_Y           = 1.0;
const double    DEFAULT_AVATAR_ROT_AXIS_Z           = 0.0;
const double    DEFAULT_AVATAR_ROT_ANGLE_DEG        = 0.0;

//avatar pose constants
const string    DEFAULT_AVATAR_POSE_NAME            = "Rest";

//avatar animation constants
//todo: determine appropriate min/max animation parameters
const double    MIN_AVATAR_ANIMATION_LENGTH         = 0.25;
const double    MAX_AVATAR_ANIMATION_LENGTH         = 5.0;
const i32       MIN_AVATAR_ANIMATION_FRAMES         = 3;
const i32       MAX_AVATAR_ANIMATION_FRAMES         = 60;
const string    DEFAULT_AVATAR_ANIMATION_NAME       = "Idle";
const double    DEFAULT_AVATAR_ANIMATION_LENGTH     = 1.0;
const i32       DEFAULT_AVATAR_ANIMATION_FRAMES     = 12;
const i32       DEFAULT_AVATAR_ANIMATION_FPS        = 12;
const i32       DEFAULT_AVATAR_ANIMATION_LOOP       = 1;

//image description constants
const string    IMAGE_TYPE_BMP                      = "bmp";
const string    IMAGE_TYPE_PNG                      = "png";
const string    IMAGE_TYPE_TIFF                     = "tiff";
const string    IMAGE_TYPE_JPEG                     = "jpeg";
const i32       MIN_IMAGE_WIDTH                     = 80;
const i32       MAX_IMAGE_WIDTH                     = 800;
const i32       MIN_IMAGE_HEIGHT                    = 60;
const i32       MAX_IMAGE_HEIGHT                    = 600;
const i32       MIN_IMAGE_QUALITY                   = 0;
const i32       MAX_IMAGE_QUALITY                   = 100;
//todo: determine appropriate min/max image dimensions
const i32       DEFAULT_IMAGE_WIDTH                 = 320;
const i32       DEFAULT_IMAGE_HEIGHT                = 240;
const string    DEFAULT_IMAGE_TYPE                  = IMAGE_TYPE_JPEG;
const i32       DEFAULT_IMAGE_QUALITY               = 85;
const i32       DEFAULT_IMAGE_SPLIT                 = 0;

//scene description constants
const double    DEFAULT_SCENE_CAMERA_POS_X          = 0.0;
const double    DEFAULT_SCENE_CAMERA_POS_Y          = 0.0;
const double    DEFAULT_SCENE_CAMERA_POS_Z          = 100.0;
const double    DEFAULT_SCENE_CAMERA_DIR_X          = 0.0;
const double    DEFAULT_SCENE_CAMERA_DIR_Y          = 0.0;
const double    DEFAULT_SCENE_CAMERA_DIR_Z          = -100.0;
const double    DEFAULT_SCENE_AMBIENT_LIGHT_R       = 0.3;
const double    DEFAULT_SCENE_AMBIENT_LIGHT_G       = 0.3;
const double    DEFAULT_SCENE_AMBIENT_LIGHT_B       = 0.3;
const double    DEFAULT_SCENE_POINT_LIGHT_POS_X     = 100.0;
const double    DEFAULT_SCENE_POINT_LIGHT_POS_Y     = 100.0;
const double    DEFAULT_SCENE_POINT_LIGHT_POS_Z     = 100.0;
