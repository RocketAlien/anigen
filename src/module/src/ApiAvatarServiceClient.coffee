{BaseAvatarServiceClient} = require './BaseAvatarServiceClient'
{transformParams, serializeParams} = require './params'

class ApiAvatarServiceClient extends BaseAvatarServiceClient
  constructor: (options, callback) ->
    super options, callback

  run: (api, params, callback) ->
    switch api
      when 'ping'       then @ping                   params, callback
      when 'kill'       then @kill                   params, callback
      when 'reload'     then @reloadAssets           params, callback
      when 'version'    then @getServerVersion       params, callback
      when 'config'     then @getServerConfig        params, callback
      when 'stats'      then @getServerStats         params, callback
      when 'avatar'     then @renderAvatars          transformParams(params), callback
      when 'pose'       then @renderAvatarPoses      transformParams(params, {pose: 1}), callback
      when 'animation'  then @renderAvatarAnimations transformParams(params, {animation: 1}), callback
      else callback "invalid api: #{api}", null

  ping: (params, callback) ->
    @getClient().ping (error) =>
      if error?
        callback error, null
      else
        callback null, 'OK\n'

  kill: (params, callback) ->
    @getClient().kill (error) =>
      if error?
        callback error, null
      else
        callback null, 'OK\n'

  reloadAssets: (params, callback) ->
    @getClient().reloadAssets (error) =>
      if error?
        callback error, null
      else
        callback null, 'OK\n'

  getServerVersion: (params, callback) ->
    @getClient().getServerVersion (error, version) =>
      if error?
        callback error, null
      else
        callback null, "#{version.versionMajor}.#{version.versionMinor}\n"

  getServerConfig: (params, callback) ->
    @getClient().getServerConfig (error, config) =>
      if error?
        callback error, null
      else
        callback null, """
          i:width=#{config.frameWidth}
          i:height=#{config.frameHeight}
          i:test=#{config.testMode}
          s:config=#{config.configFile}
          s:ogre.config=#{config.ogreConfigFile}
          s:ogre.plugins=#{config.ogrePluginsFile}
          s:ogre.resources=#{config.ogreResourcesFile}\n
        """

  getServerStats: (params, callback) ->
    @getClient().getServerStats (error, stats) =>
      if error?
        callback error, null
      else
        callback null, """
        upTime=#{stats.upTimeSeconds}sec
        renderTime=#{stats.renderTimeSeconds}sec
        numFrames=#{stats.numFrames}\n
      """

  renderAvatars: (params, callback) ->
    {avatar, image} = serializeParams(params)
    @getClient().renderAvatars avatar, image, (error, image) =>
      if error?
        callback error, null
      else
        callback null, image

  renderAvatarPoses: (params, callback) ->
    {avatar, pose, image} = serializeParams(params)
    @getClient().renderAvatarPoses avatar, pose, image, (error, image) =>
      if error?
        callback error, null
      else
        callback null, image

  renderAvatarAnimations: (params, callback) ->
    {avatar, animation, image} = serializeParams(params)
    @getClient().renderAvatarAnimations avatar, animation, image, (error, images) =>
      if error?
        callback error, null
      else
        callback null, images

exports.ApiAvatarServiceClient = ApiAvatarServiceClient
