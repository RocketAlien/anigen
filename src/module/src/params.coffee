ttypes = require './gen-nodejs/AvatarService_types'
_ = require 'underscore'
{isa, flattenKVS, unflattenKVS, transformKVS} = require 'appurilib'

val = (key) ->
  if (!key in ttypes)
    throw "unknown anigen default type: #{key}"
  ttypes[key]

# defaults maps the default values exposed by thrift into hierarchical JSON structures.
# at this moment the structure is two level deep, but it can be changed as appropriate.
# just make sure to use the pattern of naming conventions exposed by kvs.
# the higher level key name such as 'avatar', 'image', 'pose', and 'animation' still
# have meanings in the code so if they are to be changed other parts of the code still have to
# change but the effect is now a lot more localized.
defaults =
  avatar:
    height: val 'DEFAULT_AVATAR_HEIGHT'
    girth: val 'DEFAULT_AVATAR_GIRTH'
    gender: val 'DEFAULT_AVATAR_GENDER'
    bodyshape: val 'DEFAULT_AVATAR_BODYSHAPE'
    head: val 'DEFAULT_AVATAR_HEAD'
    skincolor: val 'DEFAULT_AVATAR_SKINCOLOR'
    hairstyle: val 'DEFAULT_AVATAR_HAIRSTYLE'
    haircolor: val 'DEFAULT_AVATAR_HAIRCOLOR'
    eyebrows: val 'DEFAULT_AVATAR_EYEBROWS'
    eyebrowcolor: val 'DEFAULT_AVATAR_EYEBROWCOLOR'
    eyes: val 'DEFAULT_AVATAR_EYES'
    eyecolor: val 'DEFAULT_AVATAR_EYECOLOR'
    eyeshadow: val 'DEFAULT_AVATAR_EYESHADOW'
    ears: val 'DEFAULT_AVATAR_EARS'
    nose: val 'DEFAULT_AVATAR_NOSE'
    mouth: val 'DEFAULT_AVATAR_MOUTH'
    mouthcolor: val 'DEFAULT_AVATAR_MOUTHCOLOR'
    chin: val 'DEFAULT_AVATAR_CHIN'
    facialhair: val 'DEFAULT_AVATAR_FACIALHAIR'
    facialmarks: val 'DEFAULT_AVATAR_FACIALMARKS'
    # accessories
    dress: val 'DEFAULT_AVATAR_DRESS'
    top: val 'DEFAULT_AVATAR_TOP'
    bottom: val 'DEFAULT_AVATAR_BOTTOM'
    headwear: val 'DEFAULT_AVATAR_HEADWEAR'
    shoes: val 'DEFAULT_AVATAR_SHOES'
    glasses: val 'DEFAULT_AVATAR_GLASSES'
    earrings: val 'DEFAULT_AVATAR_EARRINGS'
    necklace: val 'DEFAULT_AVATAR_NECKLACE'
    wristwear: val 'DEFAULT_AVATAR_WRISTWEAR'
    rings: val 'DEFAULT_AVATAR_RINGS'
    gloves: val 'DEFAULT_AVATAR_GLOVES'
    # position/orientation
    posX: val 'DEFAULT_AVATAR_POS_X'
    posY: val 'DEFAULT_AVATAR_POS_Y'
    posZ: val 'DEFAULT_AVATAR_POS_Z'
    rotAxisX: val 'DEFAULT_AVATAR_ROT_AXIS_X'
    rotAxisY: val 'DEFAULT_AVATAR_ROT_AXIS_Y'
    rotAxisZ: val 'DEFAULT_AVATAR_ROT_AXIS_Z'
    rotAngleDeg: val 'DEFAULT_AVATAR_ROT_ANGLE_DEG'
  pose:
    name: val 'DEFAULT_AVATAR_POSE_NAME'
  animation:
    name: val 'DEFAULT_AVATAR_ANIMATION_NAME'
    length: val 'DEFAULT_AVATAR_ANIMATION_LENGTH'
    frames: val 'DEFAULT_AVATAR_ANIMATION_FRAMES'
    fps: val 'DEFAULT_AVATAR_ANIMATION_FPS'
    loop: val 'DEFAULT_AVATAR_ANIMATION_LOOP'
  scene:
    cameraPosX: val 'DEFAULT_SCENE_CAMERA_POS_X'
    cameraPosY: val 'DEFAULT_SCENE_CAMERA_POS_Y'
    cameraPosZ: val 'DEFAULT_SCENE_CAMERA_POS_Z'
    cameraDirX: val 'DEFAULT_SCENE_CAMERA_DIR_X'
    cameraDirY: val 'DEFAULT_SCENE_CAMERA_DIR_Y'
    cameraDirZ: val 'DEFAULT_SCENE_CAMERA_DIR_Z'
    ambientLigtR: val 'DEFAULT_SCENE_AMBIENT_LIGHT_R'
    ambientLightG: val 'DEFAULT_SCENE_AMBIENT_LIGHT_G'
    ambientLightB: val 'DEFAULT_SCENE_AMBIENT_LIGHT_B'
    pointLightPosX: val 'DEFAULT_SCENE_POINT_LIGHT_POS_X'
    pointLightPosY: val 'DEFAULT_SCENE_POINT_LIGHT_POS_Y'
    pointLightPosZ: val 'DEFAULT_SCENE_POINT_LIGHT_POS_Z'
  image:
    width: val 'DEFAULT_IMAGE_WIDTH'
    height: val 'DEFAULT_IMAGE_HEIGHT'
    type: val 'DEFAULT_IMAGE_TYPE'
    quality: val 'DEFAULT_IMAGE_QUALITY'
    split: val 'DEFAULT_IMAGE_SPLIT'


defaultParams = exports.defaultParams = (types = {}) ->
  result =
    avatar:
      [ defaults.avatar ]
    image:
      defaults.image
    scene:
      defaults.scene
  if (types.animation)
    result.animation = [ defaults.animation ]
  if (types.pose)
    result.pose = [ defaults.pose ]
  result

fillDefaults = (key, obj = {}) ->
  if (!defaults[key])
    throw "unknown anigen param type: #{key}"
  result = _.extend {}, defaults[key]
  _.extend result, obj
  result

ensureSameAvatarCount = (params) ->
  count = params.avatar.length or 1
  if params.animation
    if params.animation.length != count
      throw "animation count (#{params.animation.length}) differs from avatar count (#{count})"
  if params.pose
    if params.pose.length != count
      throw "pose count (#{params.pose.length}) differs from avatar count (#{count})"

validateParams = (params, types) ->
  # fill in the defaults for params that are passed in but are not complete
  for key, val of params
    if isa(val, Array)
      params[key] =
        for v, i in val
          if (!v)
            throw "array parameter '#{key}' missing index '#{i}'"
          fillDefaults key, v
    else if (isa(val, Object))
      params[key] = fillDefaults(key, val)
    else
      params[key] = val
  # fill in the defaults for the basic objects (image, avatar, etc) that are not passed in at all
  if !params.image
    params.image = fillDefaults('image')
  if !params.avatar
    params.avatar = [ fillDefaults('avatar')  ]
  if types.pose && !params.pose
    params.pose = [ fillDefaults('pose') ]
  if types.animation && !params.animation
    params.animation = [ fillDefaults('animation') ]
  # make sure that avatar, pose, & animation all agree with each other on the .length property.
  ensureSameAvatarCount params
  params

unflattenParams = exports.unflattenParams = (flatParams, types = {}) ->
  validateParams unflattenKVS(flatParams), types

transformParams = exports.transformParams = (objParams, types = {}) ->
  validateParams transformKVS(objParams), types

serializeParams = exports.serializeParams = (params) ->
  helper = (obj) ->
    JSON.stringify(obj)
  arrayHelper = (ary) ->
    helper(obj) for obj in ary
  result = {}
  for key, val of params
    if (isa(val, Array))
      result[key] = arrayHelper val
    else if (isa(val, Object))
      result[key] = helper val
    else
      result[key] = val
  result
