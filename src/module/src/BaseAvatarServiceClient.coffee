thrift        = require 'thrift'
ttypes        = require './gen-nodejs/AvatarService_types'
AvatarService = require './gen-nodejs/AvatarService'
_ = require 'underscore'

class BaseAvatarServiceClient
  @default:
    host: 'localhost'
    port: 9090
    timeout: 10000
  #@THRIFT_TIMEOUT_MSEC: 10000

  constructor: (options, callback) ->
    @options = _.extend {} , @default
    _.extend @options, options

    # connect to the thrift server
    @connection = thrift.createConnection @options.host, @options.port, timeout: @options.timeout

    @connection.on 'error', (error) ->
      callback error

    # create a thrift client
    @client = thrift.createClient AvatarService, @connection

    # call user defined callback function
    callback null

  getClient: ->
    return @client

exports.BaseAvatarServiceClient = BaseAvatarServiceClient
