{ApiAvatarServiceClient} = require './ApiAvatarServiceClient'
{defaultParams, flattenParams, unflattenParams, transformParams} = require './params'

createClient = exports.createClient = (params, callback) ->
  new ApiAvatarServiceClient params, callback

exports.defaultParams = defaultParams
exports.flattenParams = flattenParams
exports.unflattenParams = unflattenParams
exports.transformParams = transformParams
