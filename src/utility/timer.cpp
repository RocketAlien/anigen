#include "timer.h"

#ifdef __MACH__
#include <sys/time.h>
//clock_gettime is not implemented on OSX
INLINE int clock_gettime(int /*clk_id*/, struct timespec* t) {
    struct timeval now;
    int rv = gettimeofday(&now, NULL);
    if (rv) return rv;
    t->tv_sec  = now.tv_sec;
    t->tv_nsec = now.tv_usec * 1000;
    return 0;
}
#endif //__MACH__

namespace appuri {
namespace util {

Timer::Timer() {
    memset(&start_,  0, sizeof(start_));
    memset(&finish_, 0, sizeof(finish_));
}

Timer::~Timer()
{}

void Timer::start() {
    clock_gettime(CLOCK_REALTIME, &start_);
}

void Timer::stop() {
    clock_gettime(CLOCK_REALTIME, &finish_);
}

double Timer::getTime() const {
    return (finish_.tv_sec  - start_.tv_sec) +
           (finish_.tv_nsec - start_.tv_nsec) * 0.000000001;
}

} //namespace util
} //namespace appuri
