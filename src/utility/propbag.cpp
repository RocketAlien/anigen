#include "propbag.h"
#include <sstream>
#include <jansson.h>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string/trim.hpp>

using namespace std;
using namespace boost;

LOG_DECLARE(au::LOG_FLAGS_UTILITY, "Utility",__FILE__);

namespace appuri {
namespace util {

PropertyBag::PropertyBag()
{}

PropertyBag::PropertyBag(const PropertyBag& bag) {
    update(bag);
}

PropertyBag::PropertyBag(int argc, char** argv) {
    update(argc, argv);
}

PropertyBag::PropertyBag(istream& in) {
    read(in, PropertyBag::DELIM_LINE);
}

PropertyBag::PropertyBag(const string& json) {
    fromJson(json);
}

PropertyBag::~PropertyBag()
{}

PropertyBag& PropertyBag::operator=(const PropertyBag& bag) {
    clear();
    update(bag);
    return *this;
}

bool PropertyBag::isEmpty() const {
    return strProperties_.empty() && intProperties_.empty();
}

void PropertyBag::clear() {
    strProperties_.clear();
    intProperties_.clear();
    floatProperties_.clear();
}

void PropertyBag::update(const PropertyBag& bag) {
    //copy sring properties
    for (StrPropertyMap::const_iterator it  = bag.strProperties_.begin();
                                        it != bag.strProperties_.end(); ++it) {
        strProperties_[it->first] = it->second;
    }
    //copy integer properties
    for (IntPropertyMap::const_iterator it  = bag.intProperties_.begin();
                                        it != bag.intProperties_.end(); ++it) {
        intProperties_[it->first] = it->second;
    }
    //copy float properties
    for (FloatPropertyMap::const_iterator it  = bag.floatProperties_.begin();
                                          it != bag.floatProperties_.end(); ++it) {
        floatProperties_[it->first] = it->second;
    }
}

void PropertyBag::update(int argc, char** argv) {
    stringstream ss;
    for (int i = 1; i < argc; ++i) {
        ss << argv[i] << PropertyBag::DELIM_ARG;
    }
    read(ss, PropertyBag::DELIM_ARG);
}

void PropertyBag::update(const string& json) {
    fromJson(json);
}

void PropertyBag::setStrProperty(const string& name, const string& value) {
    strProperties_[name] = value;
}

void PropertyBag::setIntProperty(const string& name, int value) {
    intProperties_[name] = value;
}

void PropertyBag::setFloatProperty(const string& name, float value) {
    floatProperties_[name] = value;
}

bool PropertyBag::hasStrProperty(const string& name) const {
    return strProperties_.find(name) != strProperties_.end();
}

bool PropertyBag::hasIntProperty(const string& name) const {
    return intProperties_.find(name) != intProperties_.end();
}

bool PropertyBag::hasFloatProperty(const string& name) const {
    return floatProperties_.find(name) != floatProperties_.end();
}

const string& PropertyBag::getStrProperty(const string& name) const {
    StrPropertyMap::const_iterator it = strProperties_.find(name);
    if (it == strProperties_.end()) {
        stringstream ss;
        ss << "Missing property: " << name;
        Throw(ss.str().c_str());
    }
    return it->second;
}

int PropertyBag::getIntProperty(const string& name) const {
    IntPropertyMap::const_iterator it = intProperties_.find(name);
    if (it == intProperties_.end()) {
        stringstream ss;
        ss << "Missing property: " << name;
        Throw(ss.str().c_str());
    }
    return it->second;
}

float PropertyBag::getFloatProperty(const string& name) const {
    FloatPropertyMap::const_iterator it = floatProperties_.find(name);
    if (it == floatProperties_.end()) {
        stringstream ss;
        ss << "Missing property: " << name;
        Throw(ss.str().c_str());
    }
    return it->second;
}

void PropertyBag::write(ostream& out, char delim) const {
    //output string properties
    for (StrPropertyMap::const_iterator it  = strProperties_.begin();
                                        it != strProperties_.end(); ++it) {
        out << "s:" << it->first << "=" << it->second << delim;
    }
    //output integer properties
    for (IntPropertyMap::const_iterator it  = intProperties_.begin();
                                        it != intProperties_.end(); ++it) {
        out << "i:" << it->first << "=" << it->second << delim;
    }
    //output float properties
    for (FloatPropertyMap::const_iterator it  = floatProperties_.begin();
                                          it != floatProperties_.end(); ++it) {
        out << "f:" << it->first << "=" << it->second << delim;
    }
}

void PropertyBag::read(istream& in, char delim) {
    string line, type, name, value;
    size_t pos, end;
    for (;;) {
        getline(in, line, delim);
        if (!in.good()) break;

        pos = line.find_first_of(':');
        if (string::npos == pos) continue;

        //type is the string before ':'
        type = line.substr(0, pos++);

        end = line.find_first_of('=', pos);
        if (string::npos == end) continue;

        //name is the string between ':' and '='
        name = line.substr(pos, end - pos);

        //value is the string after '='
        value = line.substr(++end);

        //trim leading/trailing whitespace
        trim(type);
        trim(name);
        trim(value);

        //add appropriate property type
        if (!type.compare("s")) {
            strProperties_[name] = value;
        }
        else if (!type.compare("i")) {
            intProperties_[name] = lexical_cast<int>(value);
        }
        else if (!type.compare("f")) {
            floatProperties_[name] = lexical_cast<float>(value);
        }
    }
}

bool PropertyBag::fromJson(const std::string& json) {
    DbgEnter();
    //get JSON root object
    json_t* root = json_loads(json.c_str(), 0, NULL);
    if (!json_is_object(root)) {
        DbgErr("Root object expected");
        return false;
    }
    //enumerate root object properties
    cchar* name;
    json_t* value;
    json_object_foreach(root, name, value) {
        switch (json_typeof(value)) {
            case JSON_INTEGER:
                //add integer property
                DbgMsg("Int property %s = %d", name, json_integer_value(value));
                setIntProperty(name, json_integer_value(value));
                break;

            case JSON_REAL:
                //add float property
                DbgMsg("Float property %s = %f", name, static_cast<float>(json_real_value(value)));
                setFloatProperty(name, static_cast<float>(json_real_value(value)));
                break;

            case JSON_STRING:
            default:
                //interpret all other json values as string values
                //add string property
                DbgMsg("String property %s = %s", name, json_string_value(value));
                setStrProperty(name, json_string_value(value));
                break;
        }
    }
    return true;
}

bool PropertyBag::toJson(std::string& json) const {
    DbgEnter();
    //create JSON root object
    json_t* root = json_object();
    if (!root) {
        DbgErr("Error creating root object");
        return false;
    }
    //add all properties
    json_t* value;
    //add string properties to the root object
    for (StrPropertyMap::const_iterator it  = strProperties_.begin();
                                        it != strProperties_.end(); ++it) {
        //create a string value
        value = json_string(it->second.c_str());
        if (!value) {
            DbgErr("Error creating string value");
            return false;
        }
        //add a string property to the root object
        if (json_object_set(root, it->first.c_str(), value)) {
            DbgErr("Error adding string property");
            return false;
        }
    }
    //add integer properties to the root object
    for (IntPropertyMap::const_iterator it  = intProperties_.begin();
                                        it != intProperties_.end(); ++it) {
        //create an integer value
        value = json_integer(it->second);
        if (!value) {
            DbgErr("Error creating integer value");
            return false;
        }
        //add an integer property to the root object
        if (json_object_set(root, it->first.c_str(), value)) {
            DbgErr("Error adding integer property");
            return false;
        }
    }
    //add real properties to the root object
    for (FloatPropertyMap::const_iterator it  = floatProperties_.begin();
                                          it != floatProperties_.end(); ++it) {
        //create a real value
        value = json_real(static_cast<double>(it->second));
        if (!value) {
            DbgErr("Error creating real value");
            return false;
        }
        //add a real property to the root object
        if (json_object_set(root, it->first.c_str(), value)) {
            DbgErr("Error adding real property");
            return false;
        }
    }
    //encode root object into a string
    char* jsonStr = json_dumps(root, JSON_COMPACT);
    if (!jsonStr) {
        DbgErr("Error encoding root object");
        return false;
    }
    //assign encoded string to the output string
    json = jsonStr;
    //free encoded string, this is required by the API
    free(jsonStr);
    return true;
}

ostream& operator<<(ostream& out, const PropertyBag& bag) {
    bag.write(out, PropertyBag::DELIM_LINE);
    return out;
}

istream& operator>>(istream& in, PropertyBag& bag) {
    bag.read(in, PropertyBag::DELIM_LINE);
    return in;
}

} //namespace util
} //namespace appuri
