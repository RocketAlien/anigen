#ifndef PROPERTYBAG_H
#define PROPERTYBAG_H

#include "common.h"
#include <string>
#include <iostream>
#include <boost/unordered_map.hpp>

namespace appuri {
namespace util {

typedef boost::unordered_map<std::string, std::string> StrPropertyMap;
typedef boost::unordered_map<std::string, int>         IntPropertyMap;
typedef boost::unordered_map<std::string, float>       FloatPropertyMap;

class PropertyBag {
public:
    PropertyBag();
    PropertyBag(int argc, char** argv);
    explicit PropertyBag(const PropertyBag& bag);
    explicit PropertyBag(std::istream& in);
    explicit PropertyBag(const std::string& json);
    virtual ~PropertyBag();

    PropertyBag& operator=(const PropertyBag& bag);

    bool isEmpty() const;

    void clear();

    void update(int argc, char** argv);
    void update(const PropertyBag& bag);
    void update(const std::string& json);

    void setStrProperty  (const std::string& name, const std::string& value);
    void setIntProperty  (const std::string& name, int   value);
    void setFloatProperty(const std::string& name, float value);

    bool hasStrProperty  (const std::string& name) const;
    bool hasIntProperty  (const std::string& name) const;
    bool hasFloatProperty(const std::string& name) const;

    const std::string& getStrProperty  (const std::string& name) const;
    int                getIntProperty  (const std::string& name) const;
    float              getFloatProperty(const std::string& name) const;

    void read (std::istream& in,  char delim);
    void write(std::ostream& out, char delim) const;

    bool fromJson(const std::string& json);
    bool   toJson(      std::string& json) const;

    //delimiters for processing configuration parameters
    static cchar DELIM_ARG  = ' ';
    static cchar DELIM_LINE = '\n';

private:
    StrPropertyMap   strProperties_;
    IntPropertyMap   intProperties_;
    FloatPropertyMap floatProperties_;
};

//global IO operators
std::istream& operator>>(std::istream& in,        PropertyBag& bag);
std::ostream& operator<<(std::ostream& out, const PropertyBag& bag);

} //namespace util
} //namespace appuri

#endif //PROPERTYBAG_H
