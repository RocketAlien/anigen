#ifndef TIMER_H
#define TIMER_H

#include "common.h"

namespace appuri {
namespace util {

class Timer {
public:
    Timer();
   ~Timer();

    void start();
    void stop ();

    double getTime() const;

private:
    timespec start_;
    timespec finish_;
};

} //namespace util
} //namespace appuri

#endif //TIMER_H
