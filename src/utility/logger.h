#ifndef LOGGER_H
#define LOGGER_H

#include "common.h"
#include <stdexcept>
#include <csignal>

namespace appuri {
namespace util {

//logger constants
#define LOG_PREFIX_MSG      ""
#define LOG_PREFIX_WRN      "Warning! "
#define LOG_PREFIX_ERR      "Error! "
#define LOG_PREFIX_THROW    "Throw! "
#define LOG_PREFIX_CATCH    "Catch! "

//logger flags
enum {
    LOG_FLAGS_NONE  = 0x00000000,
    LOG_FLAGS_ALL   = 0xffffffff,
};

//logger
class Log {
public:
    struct Info {
        ulong  flags_;
        cchar* title_;
        cchar* file_;
    };

    Log(const Info &info, FILE &stream,
        cchar* prefix, cchar* line, cchar* func, bool except = false);

    void write(cchar* format, ...);

    //no-op message for release builds
    INLINE static void noop(...) {}

    //global logger flags
    static ulong g_flags_;

private:
    const Info* info_;
    FILE*  stream_;
    cchar* prefix_;
    cchar* line_;
    cchar* func_;
    bool   except_;
};

} //namespace util
} //namespace appuri

//logger declaration
#define LOG_DECLARE(flags, title, file) \
static const appuri::util::Log::Info g_logInfo = {(flags), title, file}

//logger macros
#define LogMsg \
appuri::util::Log(g_logInfo,*stdout,LOG_PREFIX_MSG,STRING2(__LINE__),__FUNCTION__).write
#define LogWrn \
appuri::util::Log(g_logInfo,*stderr,LOG_PREFIX_WRN,STRING2(__LINE__),__FUNCTION__).write
#define LogErr \
appuri::util::Log(g_logInfo,*stderr,LOG_PREFIX_ERR,STRING2(__LINE__),__FUNCTION__).write
#define LogThrow \
appuri::util::Log(g_logInfo,*stderr,LOG_PREFIX_THROW,STRING2(__LINE__),__FUNCTION__,true).write
#define LogCatch \
appuri::util::Log(g_logInfo,*stderr,LOG_PREFIX_CATCH,STRING2(__LINE__),__FUNCTION__).write

//debug macros
#ifndef _DEBUG
//release build
#define DbgBreak(x)
#else
//debug build
#define DbgBreak(x)     raise(SIGTRAP)
#endif

//message/error macros
#define Msg             LogMsg
#define Wrn             LogWrn
#define Err             LogErr
#define Throw           LogThrow
#define Catch           LogCatch
#define Enter(x)        LogMsg("enter")
#define Leave(x)        LogMsg("leave")
#define Assert(b)       \
do {\
    if (!(b)) {\
        Err("Failed assertion: " STRING(b));\
        DbgBreak();\
    }\
} while (false)

//debug macros
#ifndef _DEBUG
//release build
#define DbgMsg          appuri::util::Log::noop
#define DbgWrn          appuri::util::Log::noop
#define DbgErr          appuri::util::Log::noop
#define DbgEnter(x)
#define DbgLeave(x)
#define DbgAssert(x)
#else
//debug build
#define DbgMsg          LogMsg
#define DbgWrn          LogWrn
#define DbgErr          LogErr
#define DbgEnter(x)     Enter(x)
#define DbgLeave(x)     Leave(x)
#define DbgAssert(x)    Assert(x)
#endif //_DEBUG

#endif //LOGGER_H
