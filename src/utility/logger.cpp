#include "logger.h"

using namespace std;

namespace appuri {
namespace util {

//static
ulong Log::g_flags_ = LOG_FLAGS_ALL;

Log::Log(const Info &info, FILE &stream, cchar* prefix, cchar* line, cchar* func, bool except)
: info_(&info), stream_(&stream), prefix_(prefix), line_(line), func_(func), except_(except)
{}

void Log::write(cchar* format, ...) {
    if (!(g_flags_ & info_->flags_) && !except_) {
        return;
    }
    else {
        enum { BUFFER_SIZE = MAX_STRING_SIZE * 2 };
        char buffer[BUFFER_SIZE + 1] = EMPTY_STRING;

        time_t tt = time(NULL);
        tm* t = localtime(&tt);

        uint numChars = snprintf(buffer, BUFFER_SIZE,
            "[%04u/%02u/%02u %02u:%02u:%02u] %s: %s(%s) %s(): %s",
            EPOCH_START_YEAR + t->tm_year, 1 + t->tm_mon,
            t->tm_mday, t->tm_hour, t->tm_min, t->tm_sec,
            info_->title_, info_->file_, line_, func_, prefix_);

        if (numChars < BUFFER_SIZE) {
            va_list args;
            va_start(args, format);
            vsnprintf(&buffer[numChars], BUFFER_SIZE - numChars, format, args);
            va_end(args);
        }

        buffer[BUFFER_SIZE] = NULL_CHAR;
        fprintf(stream_, "%s\n", buffer);

        if (except_) {
            //debug break here will cause an infinite signal/handle loop in debug builds
            throw runtime_error(buffer);
        }
    }
}

} //namespace util
} //namespace appuri
