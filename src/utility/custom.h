#ifndef CUSTOM_H
#define CUSTOM_H

#include "common.h"

//graphics headers
#include <GL/glew.h>

namespace appuri {
namespace util {

//error codes
enum {
    //todo: add new error codes here
};

//constants
enum {
    MAX_STRING_SIZE     = 1024,
    MAX_FILE_NAME_SIZE  = 1024,
    MAX_LINE_SIZE       = MAX_STRING_SIZE,
};
#define MAX_LINE_SIZE_STRING    STRING2(MAX_LINE_SIZE)

//logger flags
enum {
    LOG_FLAGS_UTILITY   = 0x00000001,
    LOG_FLAGS_SERVER    = 0x00000002,
    LOG_FLAGS_RENDERER  = 0x00000004,
    LOG_FLAGS_AVATAR    = 0x00000008,
    LOG_FLAGS_OGRE      = 0x00000010,
    LOG_FLAGS_ENCODER   = 0x00000020,
};

} //namespace util
} //namespace appuri

#endif //CUSTOM_H
