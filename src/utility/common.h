#ifndef COMMON_H
#define COMMON_H

//common headers
#include <cstdlib>
#include <cstdarg>
#include <cstring>
#include <cstdio>
#include <cctype>
#include <ctime>
#include <cmath>

//common macros
#ifndef NULL
#define NULL    0
#endif

#define EPOCH_START_YEAR    1900
#define NULL_CHAR           '\0'
#define EMPTY_STRING        ""

#ifndef INLINE
#define INLINE  __inline
#endif

#define IN
#define OUT
#define INOUT

#define UNUSED_PARAM(a)     ((void*)&(a))
#define EXPAND(a)           a
#define CONCAT(a, b)        a##b
#define STRING(a)           #a
#define STRING2(a)          STRING(a)
#define NUM_ELEMENTS(a)     (sizeof(a) / sizeof(a[0]))
#define BIT_FROM_INDEX(i)   (1 << (i))
#define SIGN(i)             ((i) < 0 ? -1 : 1)

namespace appuri {
namespace util {

//common functions
template<class T>
INLINE const T &MAX(const T &a, const T &b) { return a > b ? a : b; }

template<class T>
INLINE const T &MIN(const T &a, const T &b) { return a < b ? a : b; }

//common typedefs
typedef unsigned char       ubyte;
typedef unsigned char       uchar;
typedef unsigned short      ushort;
typedef unsigned int        uint;
typedef unsigned long       ulong;
typedef unsigned long long  ullong;
typedef long long           llong;
typedef const char          cchar;

//common error codes
enum {
    STATUS_OK           =  0,
    STATUS_ERR          = -1,
    ERR_BAD_INPUT       = -2,
    ERR_BAD_SIZE        = -3,
    ERR_BAD_INDEX       = -4,
    ERR_BAD_ID          = -5,
    ERR_OUT_OF_MEMORY   = -6,
};

//common constants
enum {
    BAD_SIZE    = -1,
    BAD_INDEX   = -1,
    BAD_ID      = -1,
    BAD_HASH    =  0,
};

} //namespace util
} //namespace appuri

namespace au = appuri::util;

//common headers
#include "timer.h"
#include "logger.h"
#include "propbag.h"
#include "custom.h"

#endif //COMMON_H
