#include "BaseOgreApp.h"
#include <OgreRenderSystem.h>

using namespace Ogre;
using namespace OgreBites;

namespace appuri {
namespace tools {

#define RESOURCES_FILE      "ogre.resources.tools"
#define PLUGINS_FILE        "ogre.plugins.tools"

BaseOgreApp::BaseOgreApp()
: root_             (NULL),
  window_           (NULL),
  sceneManager_     (NULL),
  camera_           (NULL),
  trayManager_      (NULL),
  cameraMan_        (NULL),
  detailsPanel_     (NULL),
  cursorWasVisible_ (false),
  shutDown_         (false),
  inputManager_     (NULL),
  mouse_            (NULL),
  keyboard_         (NULL)
{}

BaseOgreApp::~BaseOgreApp() {
    delete trayManager_;
    delete cameraMan_;
    //remove ourselves as a Window listener
    WindowEventUtilities::removeWindowEventListener(window_, this);
    windowClosed(window_);
    delete root_;
}

bool BaseOgreApp::configure() {
    if (!configureRenderer("OpenGL")) {
        return false;
    }
    root_->initialise(false, "Render Window");
    window_ = root_->createRenderWindow("Render Window", 800, 600, false);
    return true;
}

bool BaseOgreApp::configureRenderer(const String& desiredRenderer) {
    RenderSystem* renderSystem = NULL;
    const RenderSystemList& renderers = root_->getAvailableRenderers();
    if (renderers.empty()) {
        return false;
    }

    for (RenderSystemList::const_iterator it  = renderers.begin();
                                          it != renderers.end(); ++it) {
        if (strstr((*it)->getName().c_str(), desiredRenderer.c_str())) {
            renderSystem = *it;
            break;
        }
    }

    if (!renderSystem) {
        return false;
    }

    root_->setRenderSystem(renderSystem);
    //renderSystem->setConfigOption("Video Mode", "800 x 600");
    return true;
}

void BaseOgreApp::chooseSceneManager() {
    //create a generic scene manager
    sceneManager_ = root_->createSceneManager(ST_GENERIC);
}

void BaseOgreApp::createCamera() {
    //add ambient light
    sceneManager_->setAmbientLight(ColourValue(0.3, 0.3, 0.3));

    //create a camera
    camera_ = sceneManager_->createCamera("PlayerCam");
    camera_->setNearClipDistance(5);
    camera_->setFarClipDistance(1000);

    //create a flash light
    flashLight_ = sceneManager_->createLight("FlashLight");
    flashLight_->setType(Light::LT_SPOTLIGHT);

    //set camera position/orientation
    camera_->setPosition(Vector3(0, 0, 10));
    camera_->lookAt(Vector3(0, 0, -10));

    //create a default camera controller
    cameraMan_ = new SdkCameraMan(camera_);
}

void BaseOgreApp::createFrameListener() {
    LogManager::getSingletonPtr()->logMessage("*** Initializing OIS ***");
    OIS::ParamList pl;
    size_t windowHnd = 0;
    std::ostringstream windowHndStr;

    window_->getCustomAttribute("WINDOW", &windowHnd);
    windowHndStr << windowHnd;
    pl.insert(std::make_pair(std::string("WINDOW"), windowHndStr.str()));

    inputManager_ = OIS::InputManager::createInputSystem(pl);

    mouse_ = static_cast<OIS::Mouse*>(inputManager_->createInputObject(OIS::OISMouse, true));
    mouse_->setEventCallback(this);

    keyboard_ = static_cast<OIS::Keyboard*>(inputManager_->createInputObject(OIS::OISKeyboard, true));
    keyboard_->setEventCallback(this);

    //set initial mouse clipping size
    windowResized(window_);

    //register as a Window listener
    WindowEventUtilities::addWindowEventListener(window_, this);

    trayManager_ = new SdkTrayManager("InterfaceName", window_, mouse_, this);
    trayManager_->showFrameStats(TL_BOTTOMLEFT);
    trayManager_->showLogo(TL_BOTTOMRIGHT);
    trayManager_->hideCursor();

    //create a params panel for displaying sample details
    StringVector items;
    items.push_back("cam.pX");
    items.push_back("cam.pY");
    items.push_back("cam.pZ");
    items.push_back("");
    items.push_back("cam.oW");
    items.push_back("cam.oX");
    items.push_back("cam.oY");
    items.push_back("cam.oZ");
    items.push_back("");
    items.push_back("Filtering");
    items.push_back("Poly Mode");

    detailsPanel_ = trayManager_->createParamsPanel(TL_NONE, "DetailsPanel", 200, items);
    detailsPanel_->setParamValue(9,  "Bilinear");
    detailsPanel_->setParamValue(10, "Solid");
    detailsPanel_->hide();

    root_->addFrameListener(this);
}

void BaseOgreApp::destroyScene()
{}

void BaseOgreApp::createViewports() {
    //create a viewport covering entire window
    Viewport* vp = window_->addViewport(camera_);
    vp->setBackgroundColour(ColourValue(0, 0, 0));

    //alter the camera aspect ratio to match the viewport
    camera_->setAspectRatio(Real(vp->getActualWidth()) / Real(vp->getActualHeight()));
}

void BaseOgreApp::setupResources() {
    //load resource paths from config file
    ConfigFile cf;
    cf.load(RESOURCES_FILE);

    //go through all sections and settings in the file
    for (ConfigFile::SectionIterator seci = cf.getSectionIterator(); seci.hasMoreElements(); ) {
        const String& secName = seci.peekNextKey();
        ConfigFile::SettingsMultiMap* settings = seci.getNext();
        for (ConfigFile::SettingsMultiMap::iterator it  = settings->begin();
                                                    it != settings->end(); ++it) {
            const String& typeName = it->first;
            const String& archName = it->second;
            //add all media directories recursively
            ResourceGroupManager::getSingleton().addResourceLocation(
                archName, typeName, secName, true);
        }
    }
}

void BaseOgreApp::createResourceListener()
{}

void BaseOgreApp::loadResources() {
    ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
}

void BaseOgreApp::run() {
    if (!setup()) {
        return;
    }

    root_->startRendering();

    //clean up
    destroyScene();
}

bool BaseOgreApp::setup() {
    root_ = new Root(PLUGINS_FILE);

    setupResources();
    if (!configure()) {
        return false;
    }

    chooseSceneManager();
    createCamera();
    createViewports();

    TextureManager::getSingleton().setDefaultNumMipmaps(5);

    createResourceListener();
    loadResources();
    createScene();
    createFrameListener();
    return true;
}

bool BaseOgreApp::frameRenderingQueued(const FrameEvent& evt) {
    if (window_->isClosed()) {
        return false;
    }

    if (shutDown_) {
        return false;
    }

    //need to capture/update each device
    keyboard_->capture();
    mouse_->capture();

    trayManager_->frameRenderingQueued(evt);

    if (!trayManager_->isDialogVisible()) {
        //if dialog isn't up, then update the camera
        cameraMan_->frameRenderingQueued(evt);
        if (detailsPanel_->isVisible()) {
            //if details panel is visible, then update its contents
            detailsPanel_->setParamValue(0,
                StringConverter::toString(camera_->getDerivedPosition().x));
            detailsPanel_->setParamValue(1,
                StringConverter::toString(camera_->getDerivedPosition().y));
            detailsPanel_->setParamValue(2,
                StringConverter::toString(camera_->getDerivedPosition().z));
            detailsPanel_->setParamValue(4,
                StringConverter::toString(camera_->getDerivedOrientation().w));
            detailsPanel_->setParamValue(5,
                StringConverter::toString(camera_->getDerivedOrientation().x));
            detailsPanel_->setParamValue(6,
                StringConverter::toString(camera_->getDerivedOrientation().y));
            detailsPanel_->setParamValue(7,
                StringConverter::toString(camera_->getDerivedOrientation().z));
        }
        //update flash light position/orientation
        flashLight_->setPosition (camera_->getPosition());
        flashLight_->setDirection(camera_->getDirection());
    }

    return true;
}

bool BaseOgreApp::keyPressed(const OIS::KeyEvent& arg) {
    if (trayManager_->isDialogVisible()) {
        //don't process any more keys if dialog is up
        return true;
    }
        
    if (arg.key == OIS::KC_F) {
        //toggle visibility of advanced frame stats
        trayManager_->toggleAdvancedFrameStats();
    }
    else if (arg.key == OIS::KC_G) {
        //toggle visibility of even rarer debugging details
        if (detailsPanel_->getTrayLocation() == TL_NONE) {
            trayManager_->moveWidgetToTray(detailsPanel_, TL_TOPRIGHT, 0);
            detailsPanel_->show();
        }
        else {
            trayManager_->removeWidgetFromTray(detailsPanel_);
            detailsPanel_->hide();
        }
    }
    else if (arg.key == OIS::KC_T) {
        //cycle polygon rendering mode
        String newVal;
        TextureFilterOptions tfo;
        unsigned int aniso;

        switch (detailsPanel_->getParamValue(9).asUTF8()[0]) {
            case 'B':
                newVal = "Trilinear";
                tfo = TFO_TRILINEAR;
                aniso = 1;
                break;
            case 'T':
                newVal = "Anisotropic";
                tfo = TFO_ANISOTROPIC;
                aniso = 8;
                break;
            case 'A':
                newVal = "None";
                tfo = TFO_NONE;
                aniso = 1;
                break;
            default:
                newVal = "Bilinear";
                tfo = TFO_BILINEAR;
                aniso = 1;
                break;
        }

        MaterialManager::getSingleton().setDefaultTextureFiltering(tfo);
        MaterialManager::getSingleton().setDefaultAnisotropy(aniso);
        detailsPanel_->setParamValue(9, newVal);
    }
    else if (arg.key == OIS::KC_R) {
        //cycle polygon rendering mode
        String newVal;
        PolygonMode pm;

        switch (camera_->getPolygonMode()) {
            case PM_SOLID:
                newVal = "Wireframe";
                pm = PM_WIREFRAME;
                break;
            case PM_WIREFRAME:
                newVal = "Points";
                pm = PM_POINTS;
                break;
            default:
                newVal = "Solid";
                pm = PM_SOLID;
                break;
        }

        camera_->setPolygonMode(pm);
        detailsPanel_->setParamValue(10, newVal);
    }
    else if(arg.key == OIS::KC_F5) {
        //refresh all textures
        TextureManager::getSingleton().reloadAll();
    }
    else if (arg.key == OIS::KC_SYSRQ) {
        //take a screenshot
        window_->writeContentsToTimestampedFile("screenshot", ".jpg");
    }
    else if (arg.key == OIS::KC_ESCAPE) {
        shutDown_ = true;
    }

    cameraMan_->injectKeyDown(arg);
    return true;
}

bool BaseOgreApp::keyReleased(const OIS::KeyEvent& arg) {
    cameraMan_->injectKeyUp(arg);
    return true;
}

bool BaseOgreApp::mouseMoved(const OIS::MouseEvent& arg) {
    if (trayManager_->injectMouseMove(arg)) {
        return true;
    }
    cameraMan_->injectMouseMove(arg);
    return true;
}

bool BaseOgreApp::mousePressed(const OIS::MouseEvent& arg, OIS::MouseButtonID id) {
    if (trayManager_->injectMouseDown(arg, id)) {
        return true;
    }
    cameraMan_->injectMouseDown(arg, id);
    return true;
}

bool BaseOgreApp::mouseReleased(const OIS::MouseEvent& arg, OIS::MouseButtonID id) {
    if (trayManager_->injectMouseUp(arg, id)) {
        return true;
    }
    cameraMan_->injectMouseUp(arg, id);
    return true;
}

//adjust mouse clipping area
void BaseOgreApp::windowResized(RenderWindow* rw) {
    unsigned int width, height, depth;
    int left, top;
    rw->getMetrics(width, height, depth, left, top);

    const OIS::MouseState& ms = mouse_->getMouseState();
    ms.width  = width;
    ms.height = height;
}

//unattach OIS before window shutdown, very important under Linux
void BaseOgreApp::windowClosed(RenderWindow* rw) {
    //only close for window that created OIS
    if (rw == window_) {
        if (inputManager_) {
            inputManager_->destroyInputObject(mouse_);
            inputManager_->destroyInputObject(keyboard_);

            OIS::InputManager::destroyInputSystem(inputManager_);
            inputManager_ = 0;
        }
    }
}

} //namespace tools
} //namespace appuri
