Common
======
This directory contains source code, assets and configuration files common to all Ogre tools created for our asset pipeline.
BaseOgreApp.h/cpp is the base Ogre application class which can be subclassed to create custom Ogre applications.
