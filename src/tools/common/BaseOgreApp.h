#ifndef BASEOGREAPP_H
#define BASEOGREAPP_H

#include "common.h"
#include <OgreRoot.h>
#include <OgreRenderWindow.h>
#include <OgreConfigFile.h>
#include <OgreLogManager.h>
#include <OgreSceneManager.h>
#include <OgreCamera.h>
#include <OgreViewport.h>
#include <OgreEntity.h>
#include <OgreString.h>

#include <OISEvents.h>
#include <OISInputManager.h>
#include <OISKeyboard.h>
#include <OISMouse.h>

#include <SdkTrays.h>
#include <SdkCameraMan.h>

namespace appuri {
namespace tools {

class BaseOgreApp : public Ogre::FrameListener,
                    public Ogre::WindowEventListener,
                    public OIS::KeyListener,
                    public OIS::MouseListener,
                    OgreBites::SdkTrayListener {
public:
    BaseOgreApp();
    virtual ~BaseOgreApp();

    virtual void run();

protected:
    virtual bool setup();
    virtual bool configure();
    virtual bool configureRenderer(const Ogre::String& desiredRenderer);
    virtual void chooseSceneManager();
    virtual void createCamera();
    virtual void createFrameListener();

    virtual void createScene () = 0;
    virtual void destroyScene();

    virtual void createViewports();
    virtual void setupResources();
    virtual void createResourceListener();
    virtual void loadResources();

    //Ogre::FrameListener
    virtual bool frameRenderingQueued(const Ogre::FrameEvent& evt);

    //OIS::KeyListener
    virtual bool keyPressed (const OIS::KeyEvent& arg);
    virtual bool keyReleased(const OIS::KeyEvent& arg);

    //OIS::MouseListener
    virtual bool mouseMoved   (const OIS::MouseEvent& arg);
    virtual bool mousePressed (const OIS::MouseEvent& arg, OIS::MouseButtonID id);
    virtual bool mouseReleased(const OIS::MouseEvent& arg, OIS::MouseButtonID id);

    //Ogre::WindowEventListener
    virtual void windowResized(Ogre::RenderWindow* rw);
    virtual void windowClosed (Ogre::RenderWindow* rw);

    //configuration files
    Ogre::String resourcesFile_;
    Ogre::String pluginsFile_;

    //Ogre objects
    Ogre::Root*         root_;
    Ogre::RenderWindow* window_;
    Ogre::SceneManager* sceneManager_;
    Ogre::Camera*       camera_;
    Ogre::Light*        flashLight_;

    //OgreBites
    OgreBites::SdkTrayManager* trayManager_;
    OgreBites::SdkCameraMan*   cameraMan_;     //basic camera controller
    OgreBites::ParamsPanel*    detailsPanel_;  //sample details panel
    bool cursorWasVisible_;                    //was cursor visible before dialog appeared
    bool shutDown_;

    //OIS Input devices
    OIS::InputManager* inputManager_;
    OIS::Mouse*        mouse_;
    OIS::Keyboard*     keyboard_;
};

} //namespace tools
} //namespace appuri

#endif //BASEOGREAPP_H
