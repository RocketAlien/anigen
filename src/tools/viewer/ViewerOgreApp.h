#ifndef VIEWEROGREAPP_H
#define VIEWEROGREAPP_H

#include "BaseOgreApp.h"

namespace appuri {
namespace tools {

class ViewerOgreApp : public BaseOgreApp {
public:
    ViewerOgreApp(const Ogre::String& meshFile,
                  const Ogre::String& entityName,
                  const Ogre::String& animationName);
    virtual ~ViewerOgreApp();

protected:
    virtual void createScene ();
    virtual void destroyScene();
    virtual void createFrameListener();
    virtual bool frameRenderingQueued(const Ogre::FrameEvent& evt);

private:
    Ogre::String meshFile_;
    Ogre::String entityName_;
    Ogre::String animationName_;

    Ogre::Entity*          entity_;
    Ogre::AnimationState*  animationState_;
    Ogre::MeshPtr          modelMesh_;
};

} //namespace tools
} //namespace appuri

#endif //VIEWEROGREAPP_H
