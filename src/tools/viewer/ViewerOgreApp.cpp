#include "ViewerOgreApp.h"
//#include <OgreIteratorWrapper.h>
#include <OgreAnimationState.h>
#include <iostream>
#include <cstdlib>

using namespace std;
using namespace Ogre;

namespace appuri {
namespace tools {

ViewerOgreApp::ViewerOgreApp(const String& meshFile,
                             const String& entityName,
                             const String& animationName)
: meshFile_         (meshFile),
  entityName_       (entityName),
  animationName_    (animationName),
  entity_           (NULL),
  animationState_   (NULL)
{}

ViewerOgreApp::~ViewerOgreApp()
{}

void ViewerOgreApp::createScene() {
    modelMesh_ = MeshManager::getSingleton().load(meshFile_,
        ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
    entity_ = sceneManager_->createEntity(entityName_, meshFile_);
    sceneManager_->getRootSceneNode()->createChildSceneNode()->attachObject(entity_);

    /*for (SceneManager::CameraIterator it = sceneManager_->getCameraIterator();
                                      it.hasMoreElements(); ) {
        Camera* camera = it.getNext();
        cout << "HELLO CAMERA: " << camera->getName().c_str() << endl;
    }

    for (SceneManager::AnimationIterator it = sceneManager_->getAnimationIterator();
                                         it.hasMoreElements(); ) {
        Animation* animation = it.getNext();
        cout << "HELLO ANIMATION: " << animation->getName().c_str() << endl;
    }

    for (AnimationStateIterator it = sceneManager_->getAnimationStateIterator();
                                it.hasMoreElements(); ) {
        AnimationState* animationState = it.getNext();
        cout << "HELLO ANIMATION STATE: " << animationState->getAnimationName().c_str() << endl;
    }*/
}

void ViewerOgreApp::destroyScene()
{}

void ViewerOgreApp::createFrameListener() {
    BaseOgreApp::createFrameListener();

    animationState_ = entity_->getAnimationState(animationName_);
    if (animationState_) {
        animationState_->setLoop(true);
        animationState_->setEnabled(true);
    }
}

bool ViewerOgreApp::frameRenderingQueued(const FrameEvent& evt) {
    if (!BaseOgreApp::frameRenderingQueued(evt)) {
        return false;
    }

    if (animationState_) {
        animationState_->addTime(evt.timeSinceLastFrame);
    }

    return true;
}

} //namespace tools
} //namespace appuri

int main(int argc, char *argv[]) {
    enum { NUM_REQUIRED_ARGS = 4 };
    if (argc < NUM_REQUIRED_ARGS) {
        cout << "Usage: " << argv[0]
             << " <mesh-file> <entity-name> <animation-name>"
             << endl;
        return 1;
    }
    //create application object
    appuri::tools::ViewerOgreApp app(argv[1], argv[2], argv[3]);
    try {
        app.run();
    }
    catch (const Ogre::Exception& e) {
        cerr << "Ogre Exception: " << e.getFullDescription().c_str() << endl;
    }
    catch (const std::exception& e) {
        cerr << "Generic Exception: " << e.what() << endl;
    }
    return 0;
}
