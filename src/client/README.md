Overview
========
This repository contains anigen command line utility which you can run as follows.

Build and run anigen, instructions are provided separately in the anigen repository.

    $ make
    $ anigen_cli.js --host=localhost --port=9090 --api=version

Anigen CLI API Parameters
=========================
Here's a complete list of APIs, see Thrift schema for API parameters.

    --api=ping      - ping anigen server, OK on success, or error message
    --api=kill      - kill anigen server, OK on success, or error message
    --api=reload    - reload anigen assets, OK on success, or error message
    --api=version   - get server version, version on success, or error message
    --api=config    - get server configuration, configuration on success, or error message
    --api=stats     - get server statistics, statistics on success, or error message
    --api=avatar    - render avatars in default poses, image binary output to stdout, or error message
    --api=pose      - render avatars in specific poses, image binary output to stdout, or error message
    --api=animation - render avatar animations, image binary output to stdout, or error message
