{argv} = require 'optimist'
{isa}  = require 'appurilib'
fs     = require 'fs'
anigen = require 'anigen'
_      = require 'underscore'

pad4 = (number) ->
  s = '0000' + number
  s.substr(s.length - 4)

usage = () ->
  console.log 'anigen_cli --host=<host> --port=<port> --api=<api> ...'
  process.exit 0

errorCallback = (error) ->
  if error?
    console.log error
    process.exit 1

dataCallback = (error, data) ->
  if error?
    errorCallback error
  else if isa(data, Array)
    if !data.length
      errorCallback 'No image data'
    else if data.length > 1
      ext = argv.image.type or 'png'
      for i in [0...data.length]
        fs.writeFileSync "frame#{pad4(i)}.#{ext}", data[i], 'binary'
    else
      process.stdout.write data[0], 'binary'
  else
    process.stdout.write data, 'binary'
  process.exit 0

try
  client = anigen.createClient argv, errorCallback
  {api} = argv
  client.run api, argv, dataCallback
catch error
  errorCallback error
