#ifndef AVATARSERVICECONSTANTS_H
#define AVATARSERVICECONSTANTS_H

#include "AvatarService_constants.h"

#define AVATAR_SERVICE_CONST(x)     (appuri::avatar::g_AvatarService_constants.x)

#endif //AVATARSERVICECONSTANTS_H
