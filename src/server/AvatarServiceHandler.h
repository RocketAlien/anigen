#ifndef AVATARSERVICEHANDLER_H
#define AVATARSERVICEHANDLER_H

#include "common.h"
#include "AvatarService.h"
#include "AvatarRenderer.h"
#include <server/TServer.h>
#include <string>

namespace ats = apache::thrift::server;

namespace appuri {
namespace avatar {

class AvatarServiceHandler : virtual public aa::AvatarServiceIf {
public:
    explicit AvatarServiceHandler(const au::PropertyBag& cfg);
    virtual ~AvatarServiceHandler();

    void ping();
    void kill();
    void reloadAssets();

    void getServerVersion(OUT aa::ServerVersion& version);
    void getServerConfig (OUT aa::ServerConfig&  config);
    void getServerStats  (OUT aa::ServerStats&   stats);

    void renderAvatars(OUT   std::string& resultBinary,
                       const StringList&  avatarDescriptionsJson,
                       const std::string& imageDescriptionJson);

    void renderAvatarPoses(OUT   std::string& resultBinary,
                           const StringList&  avatarDescriptionsJson,
                           const StringList&  avatarPosesJson,
                           const std::string& imageDescriptionJson);

    void renderAvatarAnimations(OUT   StringList&  resultBinaryList,
                                const StringList&  avatarDescriptionsJson,
                                const StringList&  avatarAnimationsJson,
                                const std::string& imageDescriptionJson);

    //server termination
    void setServer(ats::TServer* server);

private:
    //server configuration
    const au::PropertyBag& cfg_;

    //avatar renderer
    aa::AvatarRenderer renderer_;

    //server stats
    au::Timer timer_;
    double renderTime_;
    size_t numFrames_;

    //server termination
    ats::TServer* server_;

    //scene template
    SceneDescription sceneTemplate_;

    //default avatar pose
    AvatarPose defaultAvatarPose_;

    //signal handler facility
    static AvatarServiceHandler* g_instance_;
    static void signalHandler(int signal);

    //helper functions
    double getUpTime    ();
    double getRenderTime() const;
    size_t getNumFrames () const;

    void addRenderTime(double sec);
    void addNumFrames (size_t count = 1);

    void outputConfig() const;
    void outputStats ();
};

} //namespace avatar
} //namespace appuri

#endif //AVATARSERVICEHANDLER_H
