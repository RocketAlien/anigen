#include "AvatarServiceHandler.h"
#include "AvatarServiceConstants.h"
#include <sstream>

#ifndef THRIFT_SIMPLE_SERVER
#include <protocol/TBinaryProtocol.h>
#include <server/TNonblockingServer.h>
#else
#include <transport/TServerSocket.h>
#include <transport/TBufferTransports.h>
#include <protocol/TBinaryProtocol.h>
#include <server/TSimpleServer.h>
#endif

using namespace au;
using namespace std;
using namespace apache::thrift;
using namespace apache::thrift::protocol;
using namespace apache::thrift::transport;
using namespace apache::thrift::server;
using namespace apache::thrift::concurrency;

LOG_DECLARE(LOG_FLAGS_SERVER, "Server",__FILE__);

namespace appuri {
namespace avatar {

//server constants
#define SERVER_VERSION_MAJOR    0
#define SERVER_VERSION_MINOR    1
#define SERVER_VERSION_STRING   STRING2(SERVER_VERSION_MAJOR) "." STRING2(SERVER_VERSION_MINOR)

//static
AvatarServiceHandler* AvatarServiceHandler::g_instance_ = NULL;

//static
void AvatarServiceHandler::signalHandler(int signal) {
    //call destructor manually since it is not called on exit
    Msg("Received signal %d, exiting", signal);
    g_instance_->~AvatarServiceHandler();
    exit(signal);
}

AvatarServiceHandler::AvatarServiceHandler(const PropertyBag& cfg)
: cfg_          (cfg),
  renderer_     (cfg_),
  renderTime_   (0.0),
  numFrames_    (0),
  server_       (NULL) {
    Enter();
    g_instance_ = this;

    //register signal handlers
    signal(SIGINT,  &signalHandler);
    signal(SIGTERM, &signalHandler);
    signal(SIGQUIT, &signalHandler);
    signal(SIGABRT, &signalHandler);
    signal(SIGTRAP, &signalHandler);

    signal(SIGSEGV, &signalHandler);
    signal(SIGBUS,  &signalHandler);
    signal(SIGFPE,  &signalHandler);
    signal(SIGILL,  &signalHandler);
    signal(SIGSYS,  &signalHandler);

    //ignore user logout and keep running as a daemon
    signal(SIGHUP, SIG_IGN);

    outputConfig();

    //apply scene template
    sceneTemplate_.reset();
    renderer_.setSceneDescription(sceneTemplate_);

    //initialize default avatar pose
    defaultAvatarPose_.reset();

    //start timer
    timer_.start();
}

AvatarServiceHandler::~AvatarServiceHandler() {
    Enter();
    outputConfig();
    outputStats();
    kill();
}

void AvatarServiceHandler::ping() {
    Enter();
}

void AvatarServiceHandler::kill() {
    Enter();
    DbgAssert(server_ != NULL);
    server_->stop();
}

void AvatarServiceHandler::reloadAssets() {
    Enter();
}

void AvatarServiceHandler::getServerVersion(aa::ServerVersion& version) {
    Enter();
    version.__set_versionMajor(SERVER_VERSION_MAJOR);
    version.__set_versionMinor(SERVER_VERSION_MINOR);
}

void AvatarServiceHandler::getServerConfig(aa::ServerConfig& config) {
    Enter();
    config.__set_frameWidth       (cfg_.getIntProperty("width"));
    config.__set_frameHeight      (cfg_.getIntProperty("height"));
    config.__set_testMode         (cfg_.getIntProperty("test"));
    config.__set_configFile       (cfg_.getStrProperty("config"));
    config.__set_ogreConfigFile   (cfg_.getStrProperty("ogre.config"));
    config.__set_ogrePluginsFile  (cfg_.getStrProperty("ogre.plugins"));
    config.__set_ogreResourcesFile(cfg_.getStrProperty("ogre.resources"));
}

void AvatarServiceHandler::getServerStats(aa::ServerStats& stats) {
    Enter();
    stats.__set_upTimeSeconds    (getUpTime());
    stats.__set_renderTimeSeconds(getRenderTime());
    stats.__set_numFrames        (getNumFrames());
}

void AvatarServiceHandler::renderAvatars(string& resultBinary,
                                         const StringList& avatarDescriptionsJson,
                                         const string&     imageDescriptionJson) {
    Enter();
    renderer_.clearScene();

    int size = avatarDescriptionsJson.size();
    if (size <= 0 || size > AVATAR_SERVICE_CONST(MAX_NUM_AVATARS)) {
        Throw("Avatar count = %d out of range", size);
    }

    AvatarDescriptionList avatarDescriptions(size);
    AvatarPoseList        avatarPoses       (size);

    for (int i = 0; i < size; ++i) {
        //parse avatar description
        Msg("Avatar description %d: %s", i, avatarDescriptionsJson[i].c_str());
        avatarDescriptions[i].fromJson(avatarDescriptionsJson[i]);
        //todo: perform avatar description validation

        //copy default avatar pose
        Msg("Avatar pose %d: default", i);
        avatarPoses[i].update(defaultAvatarPose_);
    }

    //apply default avatar poses
    renderer_.setAvatarPoses(avatarDescriptions, avatarPoses);

    //parse and apply image description
    Msg("Image description: %s", imageDescriptionJson.c_str());
    ImageDescription imageDescription(imageDescriptionJson);
    renderer_.setImageDescription(imageDescription);

    Timer timer;
    timer.start();

    //scene template is already applied, render scene
    renderer_.renderScene(&resultBinary);

    timer.stop();
    addRenderTime(timer.getTime());
    addNumFrames();
    Msg("Done rendering frame in %f sec", static_cast<float>(timer.getTime()));
}

void AvatarServiceHandler::renderAvatarPoses(string& resultBinary,
                                             const StringList& avatarDescriptionsJson,
                                             const StringList& avatarPosesJson,
                                             const string&     imageDescriptionJson) {
    Enter();
    renderer_.clearScene();

    int size = avatarDescriptionsJson.size();
    if (size <= 0 || size > AVATAR_SERVICE_CONST(MAX_NUM_AVATARS)) {
        Throw("Avatar count = %d out of range", size);
    }
    if (size != static_cast<int>(avatarPosesJson.size())) {
        Throw("Avatar description count = %d != avatar pose count = %u",
            size, avatarPosesJson.size());
    }

    AvatarDescriptionList avatarDescriptions(size);
    AvatarPoseList        avatarPoses       (size);

    for (int i = 0; i < size; ++i) {
        //parse avatar description
        Msg("Avatar description %d: %s", i, avatarDescriptionsJson[i].c_str());
        avatarDescriptions[i].fromJson(avatarDescriptionsJson[i]);
        //todo: perform avatar description validation

        //parse avatar pose
        Msg("Avatar pose %d: %s", i, avatarPosesJson[i].c_str());
        avatarPoses[i].fromJson(avatarPosesJson[i]);
        //todo: perform avatar pose validation
    }

    //apply avatar poses
    renderer_.setAvatarPoses(avatarDescriptions, avatarPoses);

    //parse and apply image description
    Msg("Image description: %s", imageDescriptionJson.c_str());
    ImageDescription imageDescription(imageDescriptionJson);
    renderer_.setImageDescription(imageDescription);

    Timer timer;
    timer.start();

    //scene template is already applied, render scene
    renderer_.renderScene(&resultBinary);

    timer.stop();
    addRenderTime(timer.getTime());
    addNumFrames();
    Msg("Done rendering frame in %f sec", static_cast<float>(timer.getTime()));
}

void AvatarServiceHandler::renderAvatarAnimations(      StringList& resultBinaryList,
                                                  const StringList& avatarDescriptionsJson,
                                                  const StringList& avatarAnimationsJson,
                                                  const string&     imageDescriptionJson) {
    Enter();
    renderer_.clearScene();

    int size = avatarDescriptionsJson.size();
    if (size <= 0 || size > AVATAR_SERVICE_CONST(MAX_NUM_AVATARS)) {
        Throw("Avatar count = %d out of range", size);
    }
    if (size != static_cast<int>(avatarAnimationsJson.size())) {
        Throw("Avatar description count = %d != avatar animation count = %u",
            size, avatarAnimationsJson.size());
    }

    AvatarDescriptionList avatarDescriptions(size);
    AvatarAnimationList   avatarAnimations  (size);

    for (int i = 0; i < size; ++i) {
        //parse avatar description
        Msg("Avatar description %d: %s", i, avatarDescriptionsJson[i].c_str());
        avatarDescriptions[i].fromJson(avatarDescriptionsJson[i]);
        //todo: perform avatar description validation

        //parse avatar animation
        Msg("Avatar animation %d: %s", i, avatarAnimationsJson[i].c_str());
        avatarAnimations[i].fromJson(avatarAnimationsJson[i]);
        //todo: perform avatar animation validation
    }

    //apply avatar animations
    renderer_.setAvatarAnimations(avatarDescriptions, avatarAnimations);

    //parse and apply image description
    Msg("Image description: %s", imageDescriptionJson.c_str());
    ImageDescription imageDescription(imageDescriptionJson);
    renderer_.setImageDescription(imageDescription);

    Timer timer;
    timer.start();

    //scene template is already applied, render scene
    size_t numFrames = 0;
    if (imageDescription.hasIntProperty("split") &&
        imageDescription.getIntProperty("split")) {
        //encode each frame as a separate image
        numFrames = renderer_.renderScene(NULL, &resultBinaryList);
    }
    else {
        //encode all frames as a stitched image
        resultBinaryList.clear();
        resultBinaryList.push_back(string());
        numFrames = renderer_.renderScene(&resultBinaryList[0]);
    }

    timer.stop();
    addRenderTime(timer.getTime());
    addNumFrames(numFrames);
    Msg("Done rendering %u frames in %f sec", numFrames, static_cast<float>(timer.getTime()));
}

void AvatarServiceHandler::setServer(TServer* server) {
    server_ = server;
}

double AvatarServiceHandler::getUpTime() {
    //get time elapsed since server start
    timer_.stop();
    return timer_.getTime();
}

double AvatarServiceHandler::getRenderTime() const {
    return renderTime_;
}

size_t AvatarServiceHandler::getNumFrames() const {
    return numFrames_;
}

void AvatarServiceHandler::addRenderTime(double sec) {
    renderTime_ += sec;
}

void AvatarServiceHandler::addNumFrames(size_t count) {
    numFrames_ += count;
}

void AvatarServiceHandler::outputConfig() const {
    stringstream ss;
    ss << cfg_;
    Msg("Appuri avatar rendering service");
    Msg("Server version: " SERVER_VERSION_STRING);
    Msg("Server config:\n%s", ss.str().c_str());
}

void AvatarServiceHandler::outputStats() {
    double renderTime = getRenderTime();
    size_t numFrames  = getNumFrames();
    stringstream ss;
    ss << "Server uptime: " << getUpTime() << " sec" << endl
       << "Render time: "   << renderTime  << " sec" << endl
       << "Frame count: "   << numFrames             << endl
       << "Average time: "  << (numFrames  ? renderTime / numFrames : 0.0) << " sec/img" << endl
       << "Average rate: "  << (renderTime ? numFrames / renderTime : 0.0) << " img/sec" << endl;
    Msg("Server stats:\n%s", ss.str().c_str());
}

} //namespace avatar
} //namespace appuri

void getServerConfig(int argc, char* argv[], OUT PropertyBag& cfg) {
    //set default configuration parameters
    cfg.setStrProperty("version",        SERVER_VERSION_STRING);
    cfg.setIntProperty("port",           AVATAR_SERVICE_CONST(DEFAULT_SERVER_PORT));
    cfg.setIntProperty("test",           AVATAR_SERVICE_CONST(DEFAULT_SERVER_TEST_MODE));
    cfg.setIntProperty("width",          AVATAR_SERVICE_CONST(DEFAULT_IMAGE_WIDTH));
    cfg.setIntProperty("height",         AVATAR_SERVICE_CONST(DEFAULT_IMAGE_HEIGHT));
    cfg.setStrProperty("config",         AVATAR_SERVICE_CONST(DEFAULT_SERVER_CONFIG));
    cfg.setStrProperty("ogre.config",    AVATAR_SERVICE_CONST(DEFAULT_OGRE_CONFIG));
    cfg.setStrProperty("ogre.plugins",   AVATAR_SERVICE_CONST(DEFAULT_OGRE_PLUGINS));
    cfg.setStrProperty("ogre.resources", AVATAR_SERVICE_CONST(DEFAULT_OGRE_RESOURCES));

    //get configuration parameters from command line
    PropertyBag cfgArgs(argc, argv);

    //load configuration parameters from file
    ifstream in(cfg.getStrProperty("config").c_str());
    if (in.good()) {
        //configuration file exists
        in >> cfg;
    }

    //override configuration file parameters with command line parameters
    cfg.update(cfgArgs);
}

int main(int argc, char* argv[]) {
    try {
        PropertyBag cfg;
        getServerConfig(argc, argv, cfg);

        int port = cfg.getIntProperty("port");
        if (port <= 0) {
            Throw("Invalid parameter: port = %d", port);
        }

        //create server
        boost::shared_ptr<aa::AvatarServiceHandler> handler(new aa::AvatarServiceHandler(cfg));
        boost::shared_ptr<TProcessor> processor(new aa::AvatarServiceProcessor(handler));
#ifndef THRIFT_SIMPLE_SERVER
        TNonblockingServer server(processor, port);
#else
        boost::shared_ptr<TServerTransport>  serverTransport (new TServerSocket(port));
        boost::shared_ptr<TTransportFactory> transportFactory(new TFramedTransportFactory());
        boost::shared_ptr<TProtocolFactory>  protocolFactory (new TBinaryProtocolFactory());
        TSimpleServer server(processor, serverTransport, transportFactory, protocolFactory);
#endif
        //run server
        handler->setServer(&server);
        server.serve();
    }
    catch (const std::exception& x) {
        Catch(x.what());
        return STATUS_ERR;
    }
    catch (...) {
        Catch("Unknown exception");
        return STATUS_ERR;
    }
    return STATUS_OK;
}
