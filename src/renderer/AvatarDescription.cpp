#include "AvatarDescription.h"
#include "AvatarServiceConstants.h"

using namespace au;
using namespace std;

LOG_DECLARE(LOG_FLAGS_AVATAR, "Avatar",__FILE__);

namespace appuri {
namespace avatar {

AvatarDescription::AvatarDescription() {
    DbgEnter();
}

AvatarDescription::AvatarDescription(const AvatarDescription& other)
: PropertyBag(other) {
    DbgEnter();
    *this = other;
}

AvatarDescription::AvatarDescription(const string& json) {
    DbgEnter();
    fromJson(json);
}

AvatarDescription::~AvatarDescription() {
    DbgEnter();
}

void AvatarDescription::reset() {
    DbgEnter();
    //set default property values
    //physical features
    setFloatProperty("height",          AVATAR_SERVICE_CONST(DEFAULT_AVATAR_HEIGHT));
    setFloatProperty("girth",           AVATAR_SERVICE_CONST(DEFAULT_AVATAR_GIRTH));
    setStrProperty  ("gender",          AVATAR_SERVICE_CONST(DEFAULT_AVATAR_GENDER));
    setIntProperty  ("bodyshape",       AVATAR_SERVICE_CONST(DEFAULT_AVATAR_BODYSHAPE));
    setIntProperty  ("head",            AVATAR_SERVICE_CONST(DEFAULT_AVATAR_HEAD));
    setIntProperty  ("skincolor",       AVATAR_SERVICE_CONST(DEFAULT_AVATAR_SKINCOLOR));
    setIntProperty  ("hairstyle",       AVATAR_SERVICE_CONST(DEFAULT_AVATAR_HAIRSTYLE));
    setIntProperty  ("haircolor",       AVATAR_SERVICE_CONST(DEFAULT_AVATAR_HAIRCOLOR));
    setIntProperty  ("eyebrows",        AVATAR_SERVICE_CONST(DEFAULT_AVATAR_EYEBROWS));
    setIntProperty  ("eyebrowcolor",    AVATAR_SERVICE_CONST(DEFAULT_AVATAR_EYEBROWCOLOR));
    setIntProperty  ("eyes",            AVATAR_SERVICE_CONST(DEFAULT_AVATAR_EYES));
    setIntProperty  ("eyecolor",        AVATAR_SERVICE_CONST(DEFAULT_AVATAR_EYECOLOR));
    setIntProperty  ("eyeshadow",       AVATAR_SERVICE_CONST(DEFAULT_AVATAR_EYESHADOW));
    setIntProperty  ("ears",            AVATAR_SERVICE_CONST(DEFAULT_AVATAR_EARS));
    setIntProperty  ("nose",            AVATAR_SERVICE_CONST(DEFAULT_AVATAR_NOSE));
    setIntProperty  ("mouth",           AVATAR_SERVICE_CONST(DEFAULT_AVATAR_MOUTH));
    setIntProperty  ("mouthcolor",      AVATAR_SERVICE_CONST(DEFAULT_AVATAR_MOUTHCOLOR));
    setIntProperty  ("chin",            AVATAR_SERVICE_CONST(DEFAULT_AVATAR_CHIN));
    setIntProperty  ("facialhair",      AVATAR_SERVICE_CONST(DEFAULT_AVATAR_FACIALHAIR));
    setIntProperty  ("facialmarks",     AVATAR_SERVICE_CONST(DEFAULT_AVATAR_FACIALMARKS));
    //accessories
    setIntProperty  ("dress",           AVATAR_SERVICE_CONST(DEFAULT_AVATAR_DRESS));
    setIntProperty  ("top",             AVATAR_SERVICE_CONST(DEFAULT_AVATAR_TOP));
    setIntProperty  ("bottom",          AVATAR_SERVICE_CONST(DEFAULT_AVATAR_BOTTOM));
    setIntProperty  ("shoes",           AVATAR_SERVICE_CONST(DEFAULT_AVATAR_SHOES));
    setIntProperty  ("headwear",        AVATAR_SERVICE_CONST(DEFAULT_AVATAR_HEADWEAR));
    setIntProperty  ("glasses",         AVATAR_SERVICE_CONST(DEFAULT_AVATAR_GLASSES));
    setIntProperty  ("earrings",        AVATAR_SERVICE_CONST(DEFAULT_AVATAR_EARRINGS));
    setIntProperty  ("necklace",        AVATAR_SERVICE_CONST(DEFAULT_AVATAR_NECKLACE));
    setIntProperty  ("wristwear",       AVATAR_SERVICE_CONST(DEFAULT_AVATAR_WRISTWEAR));
    setIntProperty  ("rings",           AVATAR_SERVICE_CONST(DEFAULT_AVATAR_RINGS));
    setIntProperty  ("gloves",          AVATAR_SERVICE_CONST(DEFAULT_AVATAR_GLOVES));
    //position/orientation
    setFloatProperty("posX",            AVATAR_SERVICE_CONST(DEFAULT_AVATAR_POS_X));
    setFloatProperty("posY",            AVATAR_SERVICE_CONST(DEFAULT_AVATAR_POS_Y));
    setFloatProperty("posZ",            AVATAR_SERVICE_CONST(DEFAULT_AVATAR_POS_Z));
    setFloatProperty("rotAxisX",        AVATAR_SERVICE_CONST(DEFAULT_AVATAR_ROT_AXIS_X));
    setFloatProperty("rotAxisY",        AVATAR_SERVICE_CONST(DEFAULT_AVATAR_ROT_AXIS_Y));
    setFloatProperty("rotAxisZ",        AVATAR_SERVICE_CONST(DEFAULT_AVATAR_ROT_AXIS_Z));
    setFloatProperty("rotAngleDeg",     AVATAR_SERVICE_CONST(DEFAULT_AVATAR_ROT_ANGLE_DEG));
}

} //namespace avatar
} //namespace appuri
