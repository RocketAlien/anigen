#include "AvatarPose.h"
#include "AvatarServiceConstants.h"

using namespace au;
using namespace std;

LOG_DECLARE(LOG_FLAGS_AVATAR, "Pose",__FILE__);

namespace appuri {
namespace avatar {

AvatarPose::AvatarPose() {
    DbgEnter();
}

AvatarPose::AvatarPose(const AvatarPose& other)
: PropertyBag(other) {
    DbgEnter();
    *this = other;
}

AvatarPose::AvatarPose(const string& json) {
    DbgEnter();
    fromJson(json);
}

AvatarPose::~AvatarPose() {
    DbgEnter();
}

void AvatarPose::reset() {
    DbgEnter();
    //set default property values
    setStrProperty("name",  AVATAR_SERVICE_CONST(DEFAULT_AVATAR_POSE_NAME));
}

} //namespace avatar
} //namespace appuri
