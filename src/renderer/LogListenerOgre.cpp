#include "LogListenerOgre.h"
#include <OgreLogManager.h>

using namespace au;
using namespace Ogre;

LOG_DECLARE(LOG_FLAGS_OGRE, "Ogre",__FILE__);

namespace appuri {
namespace avatar {

//log listener constants
#define DEFAULT_OGRE_LOG    "ogre.log"

OgreLogListener::OgreLogListener() {
    DbgEnter();

    //create an Ogre log manager, this creates a singleton
    LogManager* logManager = new LogManager();

    //create a default log which will not write anywhere
    DbgAssert(logManager != NULL);
    Ogre::Log* log = logManager->createLog(DEFAULT_OGRE_LOG, true, false, true);

    //set the highest log level
    DbgAssert(log != NULL);
    log->setLogDetail(LL_BOREME);

    //add log listener
    log->addListener(this);
}

void OgreLogListener::messageLogged(const String& message, LogMessageLevel level,
                                    bool, const String&, bool&) {
    //format and forward Ogre log messages to standard output
    switch (level) {
        case LML_CRITICAL:
            Err(message.c_str());
            break;
        case LML_NORMAL:
        case LML_TRIVIAL:
        default:
            Msg(message.c_str());
            break;
    }
}

} //namespace avatar
} //namespace appuri
