#include "SceneDescription.h"
#include "AvatarServiceConstants.h"

using namespace au;
using namespace std;

LOG_DECLARE(LOG_FLAGS_RENDERER, "Scene",__FILE__);

namespace appuri {
namespace avatar {

SceneDescription::SceneDescription() {
    DbgEnter();
}

SceneDescription::SceneDescription(const SceneDescription& other)
: PropertyBag(other) {
    DbgEnter();
    *this = other;
}

SceneDescription::SceneDescription(const string& json) {
    DbgEnter();
    fromJson(json);
}

SceneDescription::~SceneDescription() {
    DbgEnter();
}

void SceneDescription::reset() {
    DbgEnter();
    //set default property values
    setFloatProperty("cameraPosX",      AVATAR_SERVICE_CONST(DEFAULT_SCENE_CAMERA_POS_X));
    setFloatProperty("cameraPosY",      AVATAR_SERVICE_CONST(DEFAULT_SCENE_CAMERA_POS_Y));
    setFloatProperty("cameraPosZ",      AVATAR_SERVICE_CONST(DEFAULT_SCENE_CAMERA_POS_Z));
    setFloatProperty("cameraDirX",      AVATAR_SERVICE_CONST(DEFAULT_SCENE_CAMERA_DIR_X));
    setFloatProperty("cameraDirY",      AVATAR_SERVICE_CONST(DEFAULT_SCENE_CAMERA_DIR_Y));
    setFloatProperty("cameraDirZ",      AVATAR_SERVICE_CONST(DEFAULT_SCENE_CAMERA_DIR_Z));
    setFloatProperty("ambientLightR",   AVATAR_SERVICE_CONST(DEFAULT_SCENE_AMBIENT_LIGHT_R));
    setFloatProperty("ambientLightG",   AVATAR_SERVICE_CONST(DEFAULT_SCENE_AMBIENT_LIGHT_G));
    setFloatProperty("ambientLightB",   AVATAR_SERVICE_CONST(DEFAULT_SCENE_AMBIENT_LIGHT_B));
    setFloatProperty("pointLightPosX",  AVATAR_SERVICE_CONST(DEFAULT_SCENE_POINT_LIGHT_POS_X));
    setFloatProperty("pointLightPosY",  AVATAR_SERVICE_CONST(DEFAULT_SCENE_POINT_LIGHT_POS_Y));
    setFloatProperty("pointLightPosZ",  AVATAR_SERVICE_CONST(DEFAULT_SCENE_POINT_LIGHT_POS_Z));
}

} //namespace avatar
} //namespace appuri
