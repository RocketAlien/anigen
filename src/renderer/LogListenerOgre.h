#ifndef LOGLISTENEROGRE_H
#define LOGLISTENEROGRE_H

#include "common.h"
#include <OgreLog.h>

namespace appuri {
namespace avatar {

class OgreLogListener : public Ogre::LogListener {
public:
    OgreLogListener();

    virtual void messageLogged(const Ogre::String& message,
                               Ogre::LogMessageLevel level,
                               bool, const Ogre::String&, bool&);
};

} //namespace avatar
} //namespace appuri

#endif //LOGLISTENEROGRE_H
