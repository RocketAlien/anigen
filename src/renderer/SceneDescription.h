#ifndef SCENEDESCRIPTION_H
#define SCENEDESCRIPTION_H

#include "common.h"
#include <string>

namespace appuri {
namespace avatar {

class SceneDescription : public au::PropertyBag {
public:
    SceneDescription();
    explicit SceneDescription(const SceneDescription& other);
    explicit SceneDescription(const std::string& json);
    virtual ~SceneDescription();

    void reset();
};

} //namespace avatar
} //namespace appuri

#endif //SCENEDESCRIPTION_H
