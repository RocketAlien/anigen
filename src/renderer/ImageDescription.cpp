#include "ImageDescription.h"
#include "AvatarServiceConstants.h"

using namespace au;
using namespace std;

LOG_DECLARE(LOG_FLAGS_RENDERER, "Image",__FILE__);

namespace appuri {
namespace avatar {

ImageDescription::ImageDescription() {
    DbgEnter();
}

ImageDescription::ImageDescription(const ImageDescription& other)
: PropertyBag(other) {
    DbgEnter();
    *this = other;
}

ImageDescription::ImageDescription(const string& json) {
    DbgEnter();
    fromJson(json);
}

ImageDescription::~ImageDescription() {
    DbgEnter();
}

void ImageDescription::reset() {
    DbgEnter();
    //set default property values
    setIntProperty("width",     AVATAR_SERVICE_CONST(DEFAULT_IMAGE_WIDTH));
    setIntProperty("height",    AVATAR_SERVICE_CONST(DEFAULT_IMAGE_HEIGHT));
    setStrProperty("type",      AVATAR_SERVICE_CONST(DEFAULT_IMAGE_TYPE));
    setIntProperty("quality",   AVATAR_SERVICE_CONST(DEFAULT_IMAGE_QUALITY));
    setIntProperty("split",     AVATAR_SERVICE_CONST(DEFAULT_IMAGE_SPLIT));
}

} //namespace avatar
} //namespace appuri
