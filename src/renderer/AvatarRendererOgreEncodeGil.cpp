#include "AvatarRendererOgre.h"
#include "AvatarServiceConstants.h"
#include <boost/gil/gil_all.hpp>
#include <boost/gil/extension/io_new/bmp_all.hpp>
#include <boost/gil/extension/io_new/png_all.hpp>
#include <boost/gil/extension/io_new/tiff_all.hpp>
#include <boost/gil/extension/io_new/jpeg_all.hpp>
#include <sstream>

using namespace au;
using namespace std;
using namespace Ogre;
using namespace boost;
using namespace boost::gil;

LOG_DECLARE(LOG_FLAGS_ENCODER, "Encoder",__FILE__);

namespace appuri {
namespace avatar {

//pixel lock wrapper around a single render buffer
class RenderBufferLock {
public:
    RenderBufferLock(HardwarePixelBuffer* renderBuffer)
    : renderBuffer_(renderBuffer) {
        DbgEnter();
        lock();
    }

   ~RenderBufferLock() {
        DbgEnter();
        unlock();
    }

    INLINE void lock() {
        DbgEnter();
        DbgAssert(renderBuffer_ != NULL);
        renderBuffer_->lock(HardwareBuffer::HBL_READ_ONLY);
    }

    INLINE void unlock() {
        DbgEnter();
        DbgAssert(renderBuffer_ != NULL);
        renderBuffer_->unlock();
    }

    INLINE bgra8_pixel_t* getPixelData() {
        DbgEnter();
        DbgAssert(renderBuffer_ != NULL);
        DbgAssert(renderBuffer_->isLocked());
        //Ogre stores RGBA internally as BGRA on Linux
        return static_cast<bgra8_pixel_t*>(renderBuffer_->getCurrentLock().data);
    }

    INLINE size_t getRowSize() const {
        DbgEnter();
        DbgAssert(renderBuffer_ != NULL);
        DbgAssert(renderBuffer_->isLocked());
        return renderBuffer_->getCurrentLock().rowPitch
             * PixelUtil::getNumElemBytes(renderBuffer_->getFormat());
    }

private:
    HardwarePixelBuffer* renderBuffer_;
};

//pixel lock wrapper around a list of render buffers
class RenderBufferListLock {
public:
    RenderBufferListLock(au::uint frameWidth, au::uint frameHeight,
                         RenderBufferList& renderBuffers, au::uint numFrames)
    : frameWidth_   (frameWidth),
      frameHeight_  (frameHeight),
      numFrames_    (0),
      renderBuffers_(&renderBuffers) {
        DbgEnter();
        lock(numFrames);
    }

   ~RenderBufferListLock() {
        DbgEnter();
        unlock();
    }

    INLINE void lock(au::uint numFrames) {
        DbgEnter();
        setNumFrames(numFrames);
        for (au::uint i = 0; i < getNumFrames(); ++i) {
            HardwarePixelBuffer* renderBuffer = getRenderBuffer(i);
            DbgAssert(renderBuffer != NULL);
            DbgAssert(!renderBuffer->isLocked());
            renderBuffer->lock(HardwareBuffer::HBL_READ_ONLY);
        }
    }

    INLINE void unlock() {
        DbgEnter();
        for (au::uint i = 0; i < getNumFrames(); ++i) {
            HardwarePixelBuffer* renderBuffer = getRenderBuffer(i);
            DbgAssert(renderBuffer != NULL);
            DbgAssert(renderBuffer->isLocked());
            renderBuffer->unlock();
        }
    }

    INLINE bgra8_pixel_t getPixelAt(au::uint x, au::uint y) const {
        //todo: speed this up by using memoisation
        au::uint i = x / getFrameWidth();
        HardwarePixelBuffer* renderBuffer = getRenderBuffer(i);
        DbgAssert(renderBuffer != NULL);
        DbgAssert(renderBuffer->isLocked());
        //i = x / w, integer division, x2 = x % w = x - i * w
        //pixel at offset y * w + x2 = y * w + (x - i * w) = x + y * w - i * w = x + (y - i) * w
        return static_cast<bgra8_pixel_t*>
            (renderBuffer->getCurrentLock().data)[x + (y - i) * getFrameWidth()];
    }

private:
    au::uint frameWidth_;
    au::uint frameHeight_;
    au::uint numFrames_;
    RenderBufferList* renderBuffers_;

    INLINE au::uint getFrameWidth() const {
        return frameWidth_;
    }

    INLINE au::uint getFrameHeight() const {
        return frameHeight_;
    }

    INLINE void setNumFrames(au::uint numFrames) {
        numFrames_ = numFrames;
    }

    INLINE au::uint getNumFrames() const {
        return numFrames_;
    }

    INLINE au::uint getNumRenderBuffers() const {
        DbgAssert(renderBuffers_ != NULL);
        return renderBuffers_->size();
    }

    INLINE HardwarePixelBuffer* getRenderBuffer(au::uint frameIndex) const {
        DbgAssert(renderBuffers_ != NULL);
        DbgAssert(frameIndex < renderBuffers_->size());
        return (*renderBuffers_)[frameIndex];
    }

    RenderBufferListLock(const RenderBufferListLock& other);
    RenderBufferListLock& operator=(const RenderBufferListLock& other);
};

//GIL pixel locator wrapper around the pixel lock
class RenderBufferListFunc {
public:
    typedef RenderBufferListFunc    const_t;
    typedef point2<ptrdiff_t>       point_t;
    typedef bgra8_pixel_t           value_type;
    typedef value_type              reference;
    typedef value_type              const_reference;
    typedef point_t                 argument_type;
    typedef reference               result_type;

    BOOST_STATIC_CONSTANT(bool, is_mutable = false);

    INLINE RenderBufferListFunc() {}
    INLINE RenderBufferListFunc(RenderBufferListLock* renderBufferListLock)
    : renderBufferListLock_(renderBufferListLock) {}

    INLINE result_type operator()(const point_t& p) const {
        DbgAssert(renderBufferListLock_ != NULL);
        return renderBufferListLock_->getPixelAt(p.x, p.y);
    }

private:
    RenderBufferListLock* renderBufferListLock_;
};

void AvatarRenderer::encodeFrame(au::uint frameIndex,
                                 const ImageDescription& imageDescription,
                                 string& result) {
    DbgEnter();

    //lock the specified render buffer to get frame pixel data
    RenderBufferLock renderBufferLock(getRenderBuffer(frameIndex));

    //create a GIL interleaved image view
    bgra8_view_t imageView = interleaved_view(getFrameWidth(), getFrameHeight(),
        renderBufferLock.getPixelData(), renderBufferLock.getRowSize());

    encodeImage(imageView, imageDescription, result);
}

void AvatarRenderer::encodeFrames(au::uint numFrames,
                                  const ImageDescription& imageDescription,
                                  string& result) {
    DbgEnter();

    //lock the specified number of render buffers to get frame pixel data
    au::uint width  = getFrameWidth();
    au::uint height = getFrameHeight();
    RenderBufferListLock renderBufferListLock(width, height, renderBuffers_, numFrames);

    //stiched image width = frame width * number of frames
    width *= numFrames;

    //create a GIL image view using a GIL pixel locator
    typedef RenderBufferListFunc::point_t point_t;
    typedef virtual_2d_locator<RenderBufferListFunc, false> locator_t;

    image_view<locator_t> imageView = image_view<locator_t>(width, height,
        locator_t(point_t(0, 0), point_t(1, 1), RenderBufferListFunc(&renderBufferListLock)));

    encodeImage(imageView, imageDescription, result);
}

template<typename ImageView>
void AvatarRenderer::encodeImage(const ImageView& imageView,
                                 const ImageDescription& imageDescription,
                                 OUT string& result) {
    DbgEnter();

    //create an output memory stream
    std::stringstream out(ios::out | ios::binary);

    const string& type = imageDescription.getStrProperty("type");
    if (!type.compare(AVATAR_SERVICE_CONST(IMAGE_TYPE_JPEG))) {
        int quality = imageDescription.hasIntProperty("quality") ?
                      imageDescription.getIntProperty("quality") :
                      AVATAR_SERVICE_CONST(DEFAULT_IMAGE_QUALITY);
        if (quality < AVATAR_SERVICE_CONST(MIN_IMAGE_QUALITY) ||
            quality > AVATAR_SERVICE_CONST(MAX_IMAGE_QUALITY)) {
            DbgWrn("Image quality = %d out of range, setting to default", quality);
            quality = AVATAR_SERVICE_CONST(DEFAULT_IMAGE_QUALITY);
        }
        DbgMsg("Encoding %dx%d JPEG image at %d%% quality",
            imageView.width(), imageView.height(), quality);
        write_view(out, color_converted_view<rgb8_pixel_t>(imageView),
            image_write_info<jpeg_tag>(quality));
    }
    else if (!type.compare(AVATAR_SERVICE_CONST(IMAGE_TYPE_BMP))) {
        DbgMsg("Encoding %dx%d BMP image", imageView.width(), imageView.height());
        write_view(out, color_converted_view<rgb8_pixel_t>(imageView), bmp_tag());
    }
    else if (!type.compare(AVATAR_SERVICE_CONST(IMAGE_TYPE_TIFF))) {
        DbgMsg("Encoding %dx%d TIFF image", imageView.width(), imageView.height());
        write_view(out, color_converted_view<rgb8_pixel_t>(imageView), tiff_tag());
    }
    else { //if (!type.compare(AVATAR_SERVICE_CONST(IMAGE_TYPE_PNG))) {
        DbgMsg("Encoding %dx%d PNG image", imageView.width(), imageView.height());
        write_view(out, color_converted_view<rgb8_pixel_t>(imageView), png_tag());
    }

    //assign encoded image without incurring a copy
    result = out.str();
}

} //namespace avatar
} //namespace appuri
