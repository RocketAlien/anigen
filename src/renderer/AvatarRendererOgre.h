#ifndef AVATARRENDEREROGRE_H
#define AVATARRENDEREROGRE_H

#include "common.h"
#include "SceneDescription.h"
#include "AvatarDescription.h"
#include "AvatarPose.h"
#include "AvatarAnimation.h"
#include "ImageDescription.h"
#include "LogListenerOgre.h"
#include <OgreRoot.h>
#include <OgreRenderSystem.h>
#include <OgreRenderWindow.h>
#include <OgreRenderTexture.h>
#include <OgreHardwarePixelBuffer.h>
#include <OgreSceneManager.h>
#include <OgreCamera.h>
#include <string>
#include <vector>

namespace aa  = appuri::avatar;

namespace appuri {
namespace avatar {

typedef std::vector<std::string>                StringList;

typedef std::vector<Ogre::RenderTexture*>       RenderTextureList;
typedef std::vector<Ogre::HardwarePixelBuffer*> RenderBufferList;

typedef std::vector<aa::AvatarDescription>      AvatarDescriptionList;
typedef std::vector<aa::AvatarPose>             AvatarPoseList;
typedef std::vector<aa::AvatarAnimation>        AvatarAnimationList;

class AvatarRenderer {
public:
    explicit AvatarRenderer(const au::PropertyBag& cfg);
    virtual ~AvatarRenderer();

    //get rendering surface dimensions
    au::uint getFrameWidth () const;
    au::uint getFrameHeight() const;

    //ambient light has no position or direction and is present everywhere
    void setAmbientLight(float r, float g, float b);

    //point light has a position but shines in all directions
    void addPointLight(float posX, float posY, float posZ,
                       float diffuseR  = 1.0f,
                       float diffuseG  = 1.0f,
                       float diffuseB  = 1.0f,
                       float specularR = 1.0f,
                       float specularG = 1.0f,
                       float specularB = 1.0f);

    //directional light has no position but shines in a specific direction
    void addDirectionalLight(float dirX, float dirY, float dirZ,
                             float diffuseR  = 1.0f,
                             float diffuseG  = 1.0f,
                             float diffuseB  = 1.0f,
                             float specularR = 1.0f,
                             float specularG = 1.0f,
                             float specularB = 1.0f);

    //spot light has a specific position and shines in a specific direction
    void addSpotLight(float posX, float posY, float posZ,
                      float dirX, float dirY, float dirZ,
                      float diffuseR  = 1.0f,
                      float diffuseG  = 1.0f,
                      float diffuseB  = 1.0f,
                      float specularR = 1.0f,
                      float specularG = 1.0f,
                      float specularB = 1.0f,
                      float primaryBeamDegrees   = 30.0f,
                      float secondaryBeamDegrees = 90.0f);

    //remove all lights from the scene
    void clearLights();

    //position and orient the camera within the scene
    void setCameraPosition (float x, float y, float z);
    void setCameraDirection(float x, float y, float z);

    void setCamera(float posX, float posY, float posZ,
                   float dirX, float dirY, float dirZ);

    //reset camera to its default position and orientation
    void resetCamera();

    //apply scene description
    void setSceneDescription(const SceneDescription& sceneDescription);

    //set avatar descriptions and poses, this disables animation
    void setAvatarPoses(const AvatarDescriptionList& avatarDescriptions,
                        const AvatarPoseList&        avatarPoses);

    //set avatar descriptions and animations, this enables animation
    void setAvatarAnimations(const AvatarDescriptionList& avatarDescriptions,
                             const AvatarAnimationList&   avatarAnimations);

    //remove all objects from the scene
    void clearScene();

    //set output image format
    void setImageDescription(const ImageDescription& imageDescription);

    //render scene and retrieve scene image
    size_t renderScene(OUT std::string* result, OUT StringList* resultList = NULL);

private:
    //renderer configuration
    const au::PropertyBag& cfg_;

    //rendering surface dimensions
    au::uint width_;
    au::uint height_;

    //Ogre objects
    Ogre::Root*         root_;
    Ogre::RenderSystem* renderer_;
    Ogre::RenderWindow* renderWindow_;
    Ogre::SceneManager* sceneManager_;
    Ogre::Camera*       camera_;

    //Ogre log redirect
    OgreLogListener logListener_;

    //Ogre initialization
    bool initRenderer (au::cchar* name);
    void initResources();
    void loadResources();
    void initSceneCamera();

    //render target initialization
    void initRenderWindow  (au::cchar* title);
    void initRenderTexture (au::uint frameIndex);
    void initRenderTextures(au::uint numFrames, bool createEach);

    //todo: remove, this is temporary
    void initSceneObjects();

    //current avatars, poses and animations
    AvatarDescriptionList* avatarDescriptions_;
    AvatarPoseList*        avatarPoses_;
    AvatarAnimationList*   avatarAnimations_;

    void setAvatarDescriptions(const AvatarDescriptionList& avatarDescriptions);
    void setAvatarPoses       (const AvatarPoseList&        avatarPoses);
    void setAvatarAnimations  (const AvatarAnimationList&   avatarAnimations);

    const AvatarDescriptionList& getAvatarDescriptions() const;
    const AvatarPoseList&        getAvatarPoses       () const;
    const AvatarAnimationList&   getAvatarAnimations  () const;

    //todo: remove, this is temporary
    AvatarDescription*    avatarDescription_;
    AvatarPose*           avatarPose_;
    AvatarAnimation*      avatarAnimation_;
    Ogre::AnimationState* animationState_;

    //rendering/encoding images
    ImageDescription* imageDescription_;
    RenderTextureList renderTextures_;
    RenderBufferList  renderBuffers_;

    const ImageDescription& getImageDescription() const;

    Ogre::RenderTexture*       getRenderTexture(au::uint frameIndex);
    Ogre::HardwarePixelBuffer* getRenderBuffer (au::uint frameIndex);

    void renderFrame(au::uint frameIndex);
    void encodeFrame(au::uint frameIndex,
                     const ImageDescription& imageDescription,
                     OUT std::string& result);

    void encodeFrames(au::uint numFrames,
                      const ImageDescription& imageDescription,
                      OUT std::string& result);

    template<typename ImageView>
    void encodeImage(const ImageView& imageView,
                     const ImageDescription& imageDescription,
                     OUT std::string& result);

    //renderer state
    enum {
        FLAGS_NONE              = 0x00000000,
        FLAGS_ALL               = 0xffffffff,
        FLAGS_TEST_MODE_ENABLED = 0x00000001,
        FLAGS_ANIMATION_ENABLED = 0x00000002,
        FLAGS_SCENE_CREATED     = 0x00000004,
    };
    au::ulong flags_;

    void setFlags  (au::ulong flags, bool on);
    bool checkFlags(au::ulong flags) const;
    void resetFlags();

    void setCamera(const Ogre::Vector3& position, const Ogre::Vector3& direction);
};

} //namespace avatar
} //namespace appuri

#endif //AVATARRENDEREROGRE_H
