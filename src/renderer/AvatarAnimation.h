#ifndef AVATARANIMATION_H
#define AVATARANIMATION_H

#include "common.h"
#include <string>

namespace appuri {
namespace avatar {

class AvatarAnimation : public au::PropertyBag {
public:
    AvatarAnimation();
    explicit AvatarAnimation(const AvatarAnimation& other);
    explicit AvatarAnimation(const std::string& json);
    virtual ~AvatarAnimation();

    void reset();
};

} //namespace avatar
} //namespace appuri

#endif //AVATARANIMATION_H
