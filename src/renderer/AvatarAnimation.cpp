#include "AvatarAnimation.h"
#include "AvatarServiceConstants.h"

using namespace au;
using namespace std;

LOG_DECLARE(LOG_FLAGS_AVATAR, "Animation",__FILE__);

namespace appuri {
namespace avatar {

AvatarAnimation::AvatarAnimation() {
    DbgEnter();
}

AvatarAnimation::AvatarAnimation(const AvatarAnimation& other)
: PropertyBag(other) {
    DbgEnter();
    *this = other;
}

AvatarAnimation::AvatarAnimation(const string& json) {
    DbgEnter();
    fromJson(json);
}

AvatarAnimation::~AvatarAnimation() {
    DbgEnter();
}

void AvatarAnimation::reset() {
    DbgEnter();
    //set default property values
    setStrProperty  ("name",    AVATAR_SERVICE_CONST(DEFAULT_AVATAR_ANIMATION_NAME));
    setFloatProperty("length",  AVATAR_SERVICE_CONST(DEFAULT_AVATAR_ANIMATION_LENGTH));
    setIntProperty  ("frames",  AVATAR_SERVICE_CONST(DEFAULT_AVATAR_ANIMATION_FRAMES));
    setIntProperty  ("fps",     AVATAR_SERVICE_CONST(DEFAULT_AVATAR_ANIMATION_FPS));
    setIntProperty  ("loop",    AVATAR_SERVICE_CONST(DEFAULT_AVATAR_ANIMATION_LOOP));
}

} //namespace avatar
} //namespace appuri
