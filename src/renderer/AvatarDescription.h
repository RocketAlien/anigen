#ifndef AVATARDESCRIPTION_H
#define AVATARDESCRIPTION_H

#include "common.h"
#include <string>

namespace appuri {
namespace avatar {

class AvatarDescription : public au::PropertyBag {
public:
    AvatarDescription();
    explicit AvatarDescription(const AvatarDescription& other);
    explicit AvatarDescription(const std::string& json);
    virtual ~AvatarDescription();

    void reset();
};

} //namespace avatar
} //namespace appuri

#endif //AVATARDESCRIPTION_H
