#include "AvatarRendererOgre.h"
#include "AvatarServiceConstants.h"
#include <OgreConfigFile.h>
#include <OgreViewport.h>
#include <OgreTextureManager.h>
#include <OgreMeshManager.h>
#include <OgreEntity.h>
#include <OgreMath.h>
#include <sstream>

using namespace au;
using namespace std;
using namespace Ogre;

LOG_DECLARE(LOG_FLAGS_RENDERER, "Renderer",__FILE__);

namespace appuri {
namespace avatar {

//renderer constants
#define DEFAULT_CAMERA_DISTANCE_NEAR    0.1
#define DEFAULT_CAMERA_DISTANCE_FAR     1000
#define DEFAULT_CAMERA_POSITION         Vector3(0, -50, 100)
#define DEFAULT_CAMERA_DIRECTION        Vector3(0, 0, -100)
#define DEFAULT_TEXTURE_NUM_MIPMAPS     5

AvatarRenderer::AvatarRenderer(const PropertyBag& cfg)
: cfg_               (cfg),
  width_             (cfg_.getIntProperty("width")),
  height_            (cfg_.getIntProperty("height")),
  root_              (NULL),
  renderer_          (NULL),
  renderWindow_      (NULL),
  sceneManager_      (NULL),
  camera_            (NULL),
  avatarDescriptions_(NULL),
  avatarPoses_       (NULL),
  avatarAnimations_  (NULL),
  avatarDescription_ (NULL),
  avatarPose_        (NULL),
  avatarAnimation_   (NULL),
  animationState_    (NULL),
  imageDescription_  (NULL),
  renderTextures_    (0),
  renderBuffers_     (0),
  flags_             (FLAGS_NONE) {
    Enter();

    if (static_cast<int>(width_)  < AVATAR_SERVICE_CONST(MIN_IMAGE_WIDTH)  ||
        static_cast<int>(width_)  > AVATAR_SERVICE_CONST(MAX_IMAGE_WIDTH)  ||
        static_cast<int>(height_) < AVATAR_SERVICE_CONST(MIN_IMAGE_HEIGHT) ||
        static_cast<int>(height_) > AVATAR_SERVICE_CONST(MAX_IMAGE_HEIGHT)) {
        Throw("Invalid parameters: width = %u or height = %u", width_, height_);
    }

    setFlags(FLAGS_TEST_MODE_ENABLED, static_cast<bool>(cfg_.getIntProperty("test")));

    //initialize Ogre root object
    root_ = new Root(cfg_.getStrProperty("ogre.plugins"), cfg_.getStrProperty("ogre.config"));

    initResources();
    if (!initRenderer("OpenGL")) {
        Throw("Could not initialize OpenGL renderer");
    }

    loadResources();
}

AvatarRenderer::~AvatarRenderer() {
    Enter();
    delete root_;
}

au::uint AvatarRenderer::getFrameWidth() const {
    DbgEnter();
    return width_;
}

au::uint AvatarRenderer::getFrameHeight() const {
    DbgEnter();
    return height_;
}

void AvatarRenderer::setAmbientLight(float r, float g, float b) {
    DbgEnter();
    DbgAssert(sceneManager_ != NULL);
    sceneManager_->setAmbientLight(ColourValue(r, g, b));
}

void AvatarRenderer::addPointLight(float posX,      float posY,      float posZ,
                                   float diffuseR,  float diffuseG,  float diffuseB,
                                   float specularR, float specularG, float specularB) {
    DbgEnter();
    DbgAssert(sceneManager_ != NULL);
    Light* light = sceneManager_->createLight();
    DbgAssert(light != NULL);
    light->setType(Light::LT_POINT);
    light->setPosition(posX, posY, posZ);
    light->setDiffuseColour(diffuseR, diffuseG, diffuseB);
    light->setSpecularColour(specularR, specularG, specularB);
}

void AvatarRenderer::addDirectionalLight(float dirX,      float dirY,      float dirZ,
                                         float diffuseR,  float diffuseG,  float diffuseB,
                                         float specularR, float specularG, float specularB) {
    DbgEnter();
    DbgAssert(sceneManager_ != NULL);
    Light* light = sceneManager_->createLight();
    DbgAssert(light != NULL);
    light->setType(Light::LT_DIRECTIONAL);
    light->setDirection(dirX, dirY, dirZ);
    light->setDiffuseColour(diffuseR, diffuseG, diffuseB);
    light->setSpecularColour(specularR, specularG, specularB);
}

void AvatarRenderer::addSpotLight(float posX,      float posY,      float posZ,
                                  float dirX,      float dirY,      float dirZ,
                                  float diffuseR,  float diffuseG,  float diffuseB,
                                  float specularR, float specularG, float specularB,
                                  float primaryBeamDegrees, float secondaryBeamDegrees) {
    DbgEnter();
    DbgAssert(sceneManager_ != NULL);
    Light* light = sceneManager_->createLight();
    DbgAssert(light != NULL);
    light->setType(Light::LT_SPOTLIGHT);
    light->setPosition(posX, posY, posZ);
    light->setDirection(dirX, dirY, dirZ);
    light->setDiffuseColour(diffuseR, diffuseG, diffuseB);
    light->setSpecularColour(specularR, specularG, specularB);
    light->setSpotlightRange(Degree(primaryBeamDegrees), Degree(secondaryBeamDegrees));
}

void AvatarRenderer::clearLights() {
    DbgEnter();
    DbgAssert(sceneManager_ != NULL);
    sceneManager_->destroyAllLights();
}

void AvatarRenderer::setCameraPosition(float x, float y, float z) {
    DbgEnter();
    DbgAssert(camera_ != NULL);
    camera_->setPosition(x, y, z);
}

void AvatarRenderer::setCameraDirection(float x, float y, float z) {
    DbgEnter();
    DbgAssert(camera_ != NULL);
    camera_->lookAt(x, y, z);
}

void AvatarRenderer::setCamera(float posX, float posY, float posZ,
                               float dirX, float dirY, float dirZ) {
    DbgEnter();
    setCameraPosition (posX, posY, posZ);
    setCameraDirection(dirX, dirY, dirZ);
}

void AvatarRenderer::setCamera(const Vector3& position, const Vector3& direction) {
    DbgEnter();
    DbgAssert(camera_ != NULL);
    camera_->setPosition(position);
    camera_->lookAt(direction);
}

void AvatarRenderer::resetCamera() {
    DbgEnter();
    setCamera(DEFAULT_CAMERA_POSITION, DEFAULT_CAMERA_DIRECTION);
}

void AvatarRenderer::setSceneDescription(const SceneDescription& sd) {
    DbgEnter();

    setCamera(sd.getFloatProperty("cameraPosX"),
              sd.getFloatProperty("cameraPosY"),
              sd.getFloatProperty("cameraPosZ"),
              sd.getFloatProperty("cameraDirX"),
              sd.getFloatProperty("cameraDirY"),
              sd.getFloatProperty("cameraDirZ"));

    setAmbientLight(sd.getFloatProperty("ambientLightR"),
                    sd.getFloatProperty("ambientLightG"),
                    sd.getFloatProperty("ambientLightB"));

    if (sd.hasFloatProperty("pointLightPosX")) {
        addPointLight(sd.getFloatProperty("pointLightPosX"),
                      sd.getFloatProperty("pointLightPosY"),
                      sd.getFloatProperty("pointLightPosZ"));
    }
}

void AvatarRenderer::setAvatarPoses(const AvatarDescriptionList& avatarDescriptions,
                                    const AvatarPoseList&        avatarPoses) {
    DbgEnter();
    setAvatarDescriptions(avatarDescriptions);
    setAvatarPoses       (avatarPoses);
}

void AvatarRenderer::setAvatarAnimations(const AvatarDescriptionList& avatarDescriptions,
                                         const AvatarAnimationList&   avatarAnimations) {
    DbgEnter();
    setAvatarDescriptions(avatarDescriptions);
    setAvatarAnimations  (avatarAnimations);
}

void AvatarRenderer::setAvatarDescriptions(const AvatarDescriptionList& avatarDescriptions) {
    DbgEnter();
    //todo: load specified avatar model assets based on avatar description
    avatarDescriptions_ = const_cast<AvatarDescriptionList*>(&avatarDescriptions);
    //todo: remove, this is temporary
    DbgAssert(avatarDescriptions.size() != 0);
    avatarDescription_ = const_cast<AvatarDescription*>(&avatarDescriptions[0]);
}

void AvatarRenderer::setAvatarPoses(const AvatarPoseList& avatarPoses) {
    DbgEnter();
    //todo: load specified avatar pose and apply it to the current avatar model
    avatarPoses_ = const_cast<AvatarPoseList*>(&avatarPoses);
    //todo: remove, this is temporary
    DbgAssert(avatarPoses.size() != 0);
    avatarPose_ = const_cast<AvatarPose*>(&avatarPoses[0]);
    setFlags(FLAGS_ANIMATION_ENABLED, false);
}

void AvatarRenderer::setAvatarAnimations(const AvatarAnimationList& avatarAnimations) {
    DbgEnter();
    //todo: load specified avatar animation and apply it to the current avatar model
    avatarAnimations_ = const_cast<AvatarAnimationList*>(&avatarAnimations);
    //todo: remove, this is temporary
    DbgAssert(avatarAnimations.size() != 0);
    avatarAnimation_ = const_cast<AvatarAnimation*>(&avatarAnimations[0]);
    setFlags(FLAGS_ANIMATION_ENABLED, true);
}

const AvatarDescriptionList& AvatarRenderer::getAvatarDescriptions() const {
    DbgEnter();
    DbgAssert(avatarDescriptions_ != NULL);
    return *const_cast<const AvatarDescriptionList*>(avatarDescriptions_);
}

const AvatarPoseList& AvatarRenderer::getAvatarPoses() const {
    DbgEnter();
    DbgAssert(avatarPoses_ != NULL);
    return *const_cast<const AvatarPoseList*>(avatarPoses_);
}

const AvatarAnimationList& AvatarRenderer::getAvatarAnimations() const {
    DbgEnter();
    DbgAssert(avatarAnimations_ != NULL);
    return *const_cast<const AvatarAnimationList*>(avatarAnimations_);
}

void AvatarRenderer::clearScene() {
    DbgEnter();
    DbgAssert(sceneManager_ != NULL);
    sceneManager_->clearScene();
    //todo: remove, this is temporary
    setFlags(FLAGS_SCENE_CREATED, false);
}

void AvatarRenderer::setImageDescription(const ImageDescription& imageDescription) {
    DbgEnter();
    imageDescription_ = const_cast<ImageDescription*>(&imageDescription);
}

const ImageDescription& AvatarRenderer::getImageDescription() const {
    DbgEnter();
    DbgAssert(imageDescription_ != NULL);
    return *const_cast<const ImageDescription*>(imageDescription_);
}

size_t AvatarRenderer::renderScene(string* result, StringList* resultList) {
    DbgEnter();

    //todo: remove
    initSceneObjects();

    //todo: check if image description width and height exceed rendering surface dimensions

    //todo: animate avatar model and render multiple frames if rendering avatar animation

    //todo: check if animation is enabled and render a sequence of frames until
    //animation length is exceeded, putting image data from next frame beside
    //image data from the previous frame, and encoding the resulting sprite sheet

    if (checkFlags(FLAGS_ANIMATION_ENABLED)) {
        DbgAssert(animationState_ != NULL);
        animationState_->setEnabled(true);
        animationState_->setTimePosition(0.0f);
        animationState_->setLoop(static_cast<bool>(avatarAnimation_->getIntProperty("loop")));

        DbgAssert(avatarAnimation_ != NULL);
        //workaround for JSON libraries which encode floats with no fractional parts as integers
        float length = avatarAnimation_->hasFloatProperty("length") ?
                       avatarAnimation_->getFloatProperty("length") :
                       avatarAnimation_->getIntProperty("length");
        if (length < AVATAR_SERVICE_CONST(MIN_AVATAR_ANIMATION_LENGTH) ||
            length > AVATAR_SERVICE_CONST(MAX_AVATAR_ANIMATION_LENGTH)) {
            Throw("Animation length = %f out of range", length);
        }

        int numFrames = avatarAnimation_->getIntProperty("frames");
        if (numFrames < AVATAR_SERVICE_CONST(MIN_AVATAR_ANIMATION_FRAMES) ||
            numFrames > AVATAR_SERVICE_CONST(MAX_AVATAR_ANIMATION_FRAMES)) {
            Throw("Animation frames = %d out of range", numFrames);
        }

        if (resultList) {
            //reserve capacity to avoid vector reallocations
            resultList->clear();
            resultList->reserve(numFrames);
        }

        //render/encode a sequence of frames
        float dt = length / numFrames;
        for (int i = 0; i < numFrames; ++i) {
            DbgMsg("Rendering frame %d", i);
            animationState_->addTime(dt);
            renderFrame(i);

            if (resultList) {
                //encode each frame as a separate image
                DbgMsg("Encoding frame %d", i);
                resultList->push_back(string());
                encodeFrame(i, getImageDescription(), (*resultList)[i]);
            }
        }

        if (result) {
            //encode all frames as a stitched image
            DbgMsg("Rendering %d frames", numFrames);
            encodeFrames(numFrames, getImageDescription(), *result);
        }

        animationState_->setEnabled(false);
        return numFrames;
    }
    else if (result) {
        //render/encode a single frame
        renderFrame(0);
        encodeFrame(0, getImageDescription(), *result);
        return 1;
    }
    return 0;
}

bool AvatarRenderer::initRenderer(cchar* name) {
    DbgEnter();

    //todo: investigate directly creating an OpenGL renderer instead of loading it
    //through the plugin mechanism and then finding it among all available renderers

    //get the list of available renderers
    const RenderSystemList& renderers = root_->getAvailableRenderers();
    if (renderers.empty()) {
        return false;
    }

    //find a matching renderer
    for (RenderSystemList::const_iterator it = renderers.begin(); it != renderers.end(); ++it) {
        if (strstr((*it)->getName().c_str(), name)) {
            //found a matching renderer
            renderer_ = *it;
            break;
        }
    }

    if (!renderer_) {
        //could not find a matching renderer
        return false;
    }

    //initialize Ogre internals
    DbgAssert(root_ != NULL);
    root_->setRenderSystem(renderer_);
    root_->initialise(false, "RenderWindow");

    //set video mode to the rendering surface dimensions
    std::stringstream ss;
    ss << getFrameWidth() << " x " << getFrameHeight();
    DbgAssert(renderer_ != NULL);
    renderer_->setConfigOption("Video Mode", ss.str());

    //set RTT mode to FBO for offscreen rendering
    renderer_->setConfigOption("RTT Preferred Mode", "FBO");

    //format a window title string
    ss.str(EMPTY_STRING);
    ss <<  "w=" << getFrameWidth() << " h=" << getFrameHeight()
       << " v=" << cfg_.getStrProperty("version")
       << " t=" << static_cast<int>(checkFlags(FLAGS_TEST_MODE_ENABLED));

    //initialize onscreen render window
    initRenderWindow(ss.str().c_str());

    //initialize offscreen render textures
    //todo: determine first request time savings when creating render textures upfront
    initRenderTextures(AVATAR_SERVICE_CONST(MAX_AVATAR_ANIMATION_FRAMES), false);
    return true;
}

void AvatarRenderer::initResources() {
    DbgEnter();

    //load resource paths from the config file
    ConfigFile cf;
    cf.load(cfg_.getStrProperty("ogre.resources"));

    //go through all sections and settings in the config file
    for (ConfigFile::SectionIterator seci = cf.getSectionIterator(); seci.hasMoreElements(); ) {
        const String& secName = seci.peekNextKey();
        ConfigFile::SettingsMultiMap* settings = seci.getNext();
        for (ConfigFile::SettingsMultiMap::iterator it  = settings->begin();
                                                    it != settings->end(); ++it) {
            const String& typeName = it->first;
            const String& archName = it->second;
            //recursively add all resource subdirectories
            ResourceGroupManager::getSingleton().addResourceLocation(
                archName, typeName, secName, true);
        }
    }
}

void AvatarRenderer::loadResources() {
    //todo: load a resource group corresponding to the avatar model, pose, or animation
    //todo: unload other resources if running out of memory, implement LRU resource cache
    DbgEnter();
    TextureManager::getSingleton().setDefaultNumMipmaps(DEFAULT_TEXTURE_NUM_MIPMAPS);
    ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
    //load all resources into memory
    ResourceGroupManager::getSingleton().loadResourceGroup(
        ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
}

void AvatarRenderer::initSceneCamera() {
    DbgEnter();

    //create a generic scene manager
    DbgAssert(root_ != NULL);
    sceneManager_ = root_->createSceneManager(ST_GENERIC);

    //enable accurate stencil shadows
    DbgAssert(sceneManager_ != NULL);
    sceneManager_->setShadowTechnique(SHADOWTYPE_STENCIL_ADDITIVE);

    //create a camera
    camera_ = sceneManager_->createCamera("SceneCamera");
    DbgAssert(camera_ != NULL);
    camera_->setNearClipDistance(DEFAULT_CAMERA_DISTANCE_NEAR);
    camera_->setFarClipDistance (DEFAULT_CAMERA_DISTANCE_FAR);
    camera_->setAspectRatio(Real(getFrameWidth()) / Real(getFrameHeight()));

    //set initial camera position and orientation
    resetCamera();
}

//todo: remove this since a new scene will be created for every unique request
void AvatarRenderer::initSceneObjects() {
    DbgEnter();

    if (checkFlags(FLAGS_SCENE_CREATED)) return;

    DbgAssert(sceneManager_ != NULL);
    if (!sceneManager_->hasEntity("Robot")) {
        MeshManager::getSingleton().load(
            "robot.mesh", ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
        Entity* ent = sceneManager_->createEntity("Robot", "robot.mesh");
        DbgAssert(ent != NULL);
        ent->setCastShadows(true);

        //rotaton of theta rad around unit vector u
        //rotation quaternion q = (s,v), where s = cos(theta/2), v = sin(theta/2) * u
        //theta = -90 deg = -PI/2 rad, u = y-axis = (0,1,0)
        //s = cos(-PI/4) = cos(PI/4) = sqrt(2)/2
        //v = sin(-PI/4) * (0,1,0) = -cos(PI/4) * (0,1,0) = (0,-sqrt(2)/2,0)
        float c = Math::Cos(Math::HALF_PI / 2);
        sceneManager_->getRootSceneNode()->createChildSceneNode(
            Vector3::ZERO, Quaternion(c, 0, -c, 0))->attachObject(ent);

        //retrieve entity animation
        animationState_ = ent->getAnimationState("Walk");
    }

    if (!sceneManager_->hasEntity("Ground")) {
        //create a ground plane object
        Plane plane(Vector3::UNIT_Y, 0);
        MeshManager::getSingleton().createPlane(
            "ground", ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
            plane, 1500, 1500, 20, 20, true, 1, 5, 5, Vector3::UNIT_Z);
        Entity* ent = sceneManager_->createEntity("Ground", "ground");
        DbgAssert(ent != NULL);
        ent->setCastShadows(false);
        sceneManager_->getRootSceneNode()->createChildSceneNode()->attachObject(ent);
    }

    addPointLight(0, 100, 100);
    setCamera(-50, 100, 100, 0, 50, 0);

    setFlags(FLAGS_SCENE_CREATED, true);
}

void AvatarRenderer::initRenderWindow(cchar* title) {
    DbgEnter();

    //create an onscreen render window
    //mandatory for proper Ogre initialization, includes TextureManager singleton creation
    DbgAssert(root_ != NULL);
    renderWindow_ = root_->createRenderWindow(title, getFrameWidth(), getFrameHeight(), false);
    DbgAssert(renderWindow_ != NULL);
    renderWindow_->setAutoUpdated(false);
    //todo: create a smaller dummy render window when not in test mode

    //initialize scene manager and camera
    initSceneCamera();

    if (checkFlags(FLAGS_TEST_MODE_ENABLED)) {
        //create a viewport covering the entire render window
        DbgAssert(renderWindow_ != NULL);
        DbgAssert(camera_ != NULL);
        Viewport* vp = renderWindow_->addViewport(camera_);
        DbgAssert(vp != NULL);
        vp->setBackgroundColour(ColourValue::Black);
        vp->setClearEveryFrame(true);
        vp->setOverlaysEnabled(true);
    }
}

void AvatarRenderer::initRenderTexture(au::uint frameIndex) {
    DbgEnter();

    //check if creating a new render texture is necessary
    if (frameIndex < renderTextures_.size()) return;

    //create an offscreen texture
    std::stringstream ss;
    ss << "RenderTexture" << frameIndex;
    TexturePtr texture = TextureManager::getSingleton().createManual(
        ss.str(), ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
        TEX_TYPE_2D, getFrameWidth(), getFrameHeight(), 0, PF_BYTE_RGBA, TU_RENDERTARGET);

    //get underlying render buffer and render texture
    HardwarePixelBuffer* renderBuffer = texture->getBuffer().get();
    DbgAssert(renderBuffer != NULL);
    RenderTexture* renderTexture = renderBuffer->getRenderTarget();
    DbgAssert(renderTexture != NULL);
    renderTexture->setAutoUpdated(false);

    //create a viewport covering the entire render texture
    DbgAssert(camera_ != NULL);
    Viewport* vp = renderTexture->addViewport(camera_);
    DbgAssert(vp != NULL);
    vp->setBackgroundColour(ColourValue::Black);
    vp->setClearEveryFrame(true);
    vp->setOverlaysEnabled(false);

    //add newly created render texture and render buffer
    renderTextures_.push_back(renderTexture);
    renderBuffers_.push_back(renderBuffer);
}

void AvatarRenderer::initRenderTextures(au::uint numFrames, bool createEach) {
    DbgEnter();

    //this method should only be called once during renderer initialization
    DbgAssert(renderTextures_.capacity() == 0);
    DbgAssert(renderBuffers_.capacity() == 0);

    //reserve capacity to avoid vector reallocations
    renderTextures_.reserve(numFrames);
    renderBuffers_.reserve(numFrames);

    //initialize individual render textures if necessary
    if (createEach) {
        for (au::uint i = 0; i < numFrames; ++i) {
            initRenderTexture(i);
        }
    }
}

RenderTexture* AvatarRenderer::getRenderTexture(au::uint frameIndex) {
    DbgEnter();
    initRenderTexture(frameIndex);
    DbgAssert(frameIndex < renderTextures_.size());
    return renderTextures_[frameIndex];
}

HardwarePixelBuffer* AvatarRenderer::getRenderBuffer(au::uint frameIndex) {
    DbgEnter();
    initRenderTexture(frameIndex);
    DbgAssert(frameIndex < renderBuffers_.size());
    return renderBuffers_[frameIndex];
}

void AvatarRenderer::renderFrame(au::uint frameIndex) {
    DbgEnter();

    if (checkFlags(FLAGS_TEST_MODE_ENABLED)) {
        //update onscreen render window
        DbgAssert(renderWindow_ != NULL);
        renderWindow_->update();
    }

    //update offscreen render texture
    RenderTexture* renderTexture = getRenderTexture(frameIndex);
    DbgAssert(renderTexture != NULL);
    renderTexture->update();

    //draw all render targets
    DbgAssert(root_ != NULL);
    root_->renderOneFrame();
}

void AvatarRenderer::setFlags(au::ulong flags, bool on) {
    DbgEnter();
    if (on) {
        flags_ |= flags;
    }
    else {
        flags_ &= ~flags;
    }
}

bool AvatarRenderer::checkFlags(au::ulong flags) const {
    DbgEnter();
    return (flags_ & flags) == flags;
}

void AvatarRenderer::resetFlags() {
    DbgEnter();
    flags_ = FLAGS_NONE;
}

} //namespace avatar
} //namespace appuri
