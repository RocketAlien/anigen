#ifndef IMAGEDESCRIPTION_H
#define IMAGEDESCRIPTION_H

#include "common.h"
#include <string>

namespace appuri {
namespace avatar {

class ImageDescription : public au::PropertyBag {
public:
    ImageDescription();
    explicit ImageDescription(const ImageDescription& other);
    explicit ImageDescription(const std::string& json);
    virtual ~ImageDescription();

    void reset();
};

} //namespace avatar
} //namespace appuri

#endif //IMAGEDESCRIPTION_H
