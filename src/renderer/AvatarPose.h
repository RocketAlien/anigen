#ifndef AVATARPOSE_H
#define AVATARPOSE_H

#include "common.h"
#include <string>

namespace appuri {
namespace avatar {

class AvatarPose : public au::PropertyBag {
public:
    AvatarPose();
    explicit AvatarPose(const AvatarPose& other);
    explicit AvatarPose(const std::string& json);
    virtual ~AvatarPose();

    void reset();
};

} //namespace avatar
} //namespace appuri

#endif //AVATARPOSE_H
